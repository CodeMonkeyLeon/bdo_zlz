package cn.swiftpass.enterprise.bean

import java.io.Serializable

data class BarPositionEntity(
    val x: Float = 0f,
    val y: Float = 0f,
    val width: Float = 0f,
    val height: Float = 0f
) : Serializable {


    fun getRectLeft() = x

    fun getRectTop() = y + getCircleR()

    fun getRectRight() = x + width

    fun getRectBottom() = y + height - getCircleR()


    fun getCircleTopX() = x + getCircleR()

    fun getCircleTopY() = y + getCircleR()

    fun getCircleBottomX() = x + getCircleR()

    fun getCircleBottomY() = y + height - getCircleR()


    fun getCircleR() = width / 2
}
