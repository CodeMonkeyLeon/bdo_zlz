package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

import cn.swiftpass.enterprise.io.database.table.ShopTable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * 店铺实体
 * User: Administrator
 * Date: 13-12-23
 * Time: 下午1:59
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = ShopTable.TABLE_NAME)
public class ShopModel implements Serializable
{
    @DatabaseField(generatedId = true)
    public long id;
    
    @DatabaseField(columnName = ShopTable.COLUMN_SID)
    public Integer shopId;
    
    @DatabaseField(columnName = ShopTable.COLUMN__MID)
    public Integer merchantId; //商户表id即：MerchantModel 中的id（不是 merchantId属性）
    
    @DatabaseField(columnName = ShopTable.COLUMN__NAME)
    public String shopName;
    
    @DatabaseField(columnName = ShopTable.COLUMN__CITY)
    public String city; //所在城市
    
    @DatabaseField(columnName = ShopTable.COLUMN_PIC_PATH)
    public String shopPic; //店铺图标
    
    @DatabaseField(columnName = ShopTable.COLUMN__SHOP_AMOUN)
    public Integer shopAmount; //店铺优惠劵种类
    
    @DatabaseField(columnName = ShopTable.COLUMN__SHOP_TYPE)
    public Integer shopTypeId; //店铺类型 ShopTypeEnum
    
    @DatabaseField(columnName = ShopTable.COLUMN__ADDRESS)
    public String address; //店铺地址
    
    @DatabaseField(columnName = ShopTable.COLUMN__LINK_PHONE)
    public String linkPhone; //联系电话
    
    //@DatabaseField( columnName = ShopTable.COLUMN__START_TIME,dataType = DataType.DATE_TIME)
    public String startTime; //营业开始时间
    
    // @DatabaseField( columnName = ShopTable.COLUMN__END_TIME,dataType = DataType.DATE_TIME)
    public String endTime; //营业结束时间
    
    @DatabaseField(columnName = ShopTable.COLUMN__STR_START_TIME)
    public String strStartTime;
    
    @DatabaseField(columnName = ShopTable.COLUMN__STR_END_TIME)
    public String strEndTime;
    
    @DatabaseField(columnName = ShopTable.COLUMN__TRAFFIC_INFO)
    public String trafficInfo; //交通信息
    
    @DatabaseField(columnName = ShopTable.COLUMN__ATTACH_INFO)
    public String attachInfo; //附加信息
    
    @DatabaseField(columnName = ShopTable.COLUMN__REMARK)
    public String remark; //备注
    
    //@DatabaseField( columnName = ShopTable.COLUMN_PIC_PATH)
    //public String picPath;//路径名称
    
    @DatabaseField(columnName = ShopTable.COLUMN_UID)
    public Long uId; //用户ID
    
    @DatabaseField(columnName = ShopTable.COLUMN_ADD_TIME)
    public Long addTime;
    
}
