package cn.swiftpass.enterprise.bussiness.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-19
 * Time: 下午6:13
 * To change this template use File | Settings | File Templates.
 */
public enum UserRole
{
    /**
     * 管理账号
     */
    manager(1, "管理账号"), approve(2, "管理账号"), //目前系统只有审核员也是管理员账号
    general(0, "普通用户"), ;
    
    private Integer enumId;
    
    private String displayName;
    
    private UserRole(Integer enumId, String displayName)
    {
        this.displayName = displayName;
        this.enumId = enumId;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public Integer getEnumId()
    {
        return this.enumId;
    }
}
