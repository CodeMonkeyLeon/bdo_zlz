package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.List;

public class AwardModelList implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private AwardModel awardModel;
    
    private List<AwardModel> awardModelList;
    
    /**
     * @return 返回 awardModel
     */
    public AwardModel getAwardModel()
    {
        return awardModel;
    }
    
    /**
     * @param 对awardModel进行赋值
     */
    public void setAwardModel(AwardModel awardModel)
    {
        this.awardModel = awardModel;
    }
    
    /**
     * @return 返回 awardModelList
     */
    public List<AwardModel> getAwardModelList()
    {
        return awardModelList;
    }
    
    /**
     * @param 对awardModelList进行赋值
     */
    public void setAwardModelList(List<AwardModel> awardModelList)
    {
        this.awardModelList = awardModelList;
    }
    
}
