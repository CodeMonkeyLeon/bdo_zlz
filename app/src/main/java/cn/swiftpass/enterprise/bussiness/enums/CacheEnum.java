package cn.swiftpass.enterprise.bussiness.enums;

/**
 * 缓存类型.
 * User: Administrator
 * Date: 13-11-18
 * Time: 下午6:23
 */
public enum CacheEnum {

       CASHIER_THIS_WEEK_TYPE(0,"this_week_o_data"),//"本周统计缓存"
       CASHIER_LAST_WEEK_TYPE(1,"last_week_o_data"),//"上周统计缓存"
       CASHIER_THIS_MONTH_TYPE(2,"this_month_o_data"),//"本月统计缓存"
       CASHIER_LAST_MONTH_TYPE(3,"last_month_o_data"),//"上月统计缓存"
       CASHIER_DATE_TIME_TYPE(6,"date_time"),//任意时间
       ORDER_TYPE(4,"订单缓存") ,
       SHOP_TYPE(5,"店铺缓存") ;

    private final Integer value;
    private String displayName = "";

    private CacheEnum(Integer v ,String displayName) {
        this.value = v;
        this.displayName = displayName;
    }

    public static String getDisplayNameByVaue(Integer v){
        if(v!=null){
            for (CacheEnum ose : values()) {
                if (ose.value.equals(v)){
                    return ose.getDisplayName();
                }
            }
        }
        return "";
    }
    public String getDisplayName() {
        return displayName;
    }

    public Integer getValue() {
        return value;
    }
    public static String getName(int state)
    {
        if(state == CacheEnum.CASHIER_THIS_WEEK_TYPE.getValue())
        {
           return CacheEnum.CASHIER_THIS_WEEK_TYPE.getDisplayName();
        }else if(state == CacheEnum.CASHIER_LAST_WEEK_TYPE.getValue()){
           return CacheEnum.CASHIER_LAST_WEEK_TYPE.getDisplayName();
        }else if(state == CacheEnum.CASHIER_THIS_MONTH_TYPE.getValue()){
            return CacheEnum.CASHIER_THIS_MONTH_TYPE.getDisplayName();
        }else if(state == CacheEnum.CASHIER_LAST_MONTH_TYPE.getValue()){
            return CacheEnum.CASHIER_LAST_MONTH_TYPE.getDisplayName();
        }

        return null;
    }
}
