package cn.swiftpass.enterprise.bussiness.logica.common;


public abstract class DataObserver
{
	

	public static final String EVENT_LOAD_APP_ORDER = "event.LOAD_APP_ORDER";
	/**服务器错误*/
	public static final String EVENT_SERVER_ERROR = "event.SERVER_ERROR";
	/**未登录*/
	public static final String EVENT_LOGIN_CHECKIN = "event.LOGIN_CHECKIN";
	private String consumerName;
	private String event;
	public static final String EVENT_LOAD_MY_APP = "event.LOAD_MYAPP";
	public static final String EVENT_LOAD_MY_APP_GIVE = "event.LOAD_MYAPP_GIVE";
    /**订单状态发送改变*/
    public static final String EVENT_ORDER_STATE_CHANGE="event.order_state_change";
	/**
	 * 网络断开
	 */
	public static final String EVENT_NETWORK_CHANGER_DESTORY= "event.NETWORK_DISTORY";
	
	/**
	 *网络连接
	 */
	public static final String EVENT_NETWORK_CHANGER_COMMEND= "event.NETWORK_COMMEND";
    /**
     *登录失败，需要进行设备激活提示
     */
    public static final String EVENT_LOGIN_FAIL_MAKE_DEVICE= "event.LOGIN_FAIL_MAKE_DEVICE";
    /**
     *登录失败，需要是否需要离线交易
     */
    public static final String EVENT_LOGIN_FAIL_OFFLINE_PAY= "event.LOGIN_FAIL_OFFLINE_PAY";
    /**
     *登录状态失效
     */
    public static final String EVENT_USER_INVALI= "event.USER_INVALI";
    /**
     *优惠券修改
     */
    public static final String EVENT_COUPON_EDIT_CHANGE= "event.COUPON_EDIT_CHANGE";
	/**
	 * 哪个类注册的
	 * @return
	 */
	public String getConsumerName()
	{
		return consumerName;
	}

	/**
	 * 观察哪个表
	 * @return
	 */
	public String getEventName()
	{
		return event;
	}
	
	public DataObserver(String consumerName, String event)
	{
		this.consumerName = consumerName;
		this.event = event;
	}
	public abstract void onChange();
}
