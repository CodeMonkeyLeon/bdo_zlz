package cn.swiftpass.enterprise.bussiness.logica.download.inteferace;


import cn.swiftpass.enterprise.bussiness.model.DownloadInfo;

public interface IDownloadManager {
	
	public void startManage();
	public void stopManage();
	
	public void startADownLoad(DownloadInfo info);
	public void startADownLoad(String name, String url, long id, String suffix);
	
	public void pauseADownLoad(long id);
	
	public void continueADownLoad(long id);
	
	public void cancelADownLoad(long id);
	
	
	
	public void pauseAllDownload();
	public void continueAllDownload();
	public void cancelAllDownload();
	
	public void setMaxDownloadTaskCount(int count);
	
/*	public List<DownloadInfo> getReadyAndDownloadingList();
	public List<DownloadInfo> getDownloadPausedList();
	public List<DownloadInfo> getDownloadCompleteList();*/
}
