package cn.swiftpass.enterprise.bussiness.model;

import android.bluetooth.BluetoothDevice;

/**
 * 蓝牙设备
 * User: Alan
 * Date: 14-1-20
 * Time: 下午5:37
 * To change this template use File | Settings | File Templates.
 */
public class BlueDevice {
        private String deviceName;
        private String address;
        private int bondState;
        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

    public String getBondState() {
         switch (bondState)
         {
             case BluetoothDevice.BOND_BONDED:
                 return "已匹配";
             case BluetoothDevice.BOND_BONDING:
                 return "匹配中";
             case BluetoothDevice.BOND_NONE:
                 return "未匹配";
             case BluetoothDevice.ERROR:
                 return "异常";
         }
        return "";
    }

    public void setBondState(int bondState) {
        this.bondState = bondState;
    }
}
