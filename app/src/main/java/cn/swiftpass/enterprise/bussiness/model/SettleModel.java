/*
 * 文 件 名:  SettleModel.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-7-19
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;


/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2016-7-19]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
@SuppressWarnings("unused")
public class SettleModel
{
    
    /** {@inheritDoc} */
    
    @Override
    public String toString()
    {
        return "SettleModel [coupon_total_count=" + coupon_total_count + ", coupon_total_fee=" + coupon_total_fee
            + ", refund_total_count=" + refund_total_count + ", refund_total_fee=" + refund_total_fee
            + ", success_total_count=" + success_total_count + ", success_total_fee=" + success_total_fee
            + ", total_net_fee=" + total_net_fee + "]";
    }
    
    private String coupon_total_count; // 优惠订单笔数
    
    private String coupon_total_fee; // 优惠订单金额
    
    private String refund_total_count; // 总退款笔数
    
    /**
     * @return 返回 coupon_total_count
     */
    public String getCoupon_total_count()
    {
        return coupon_total_count;
    }
    
    /**
     * @param 对coupon_total_count进行赋值
     */
    public void setCoupon_total_count(String coupon_total_count)
    {
        this.coupon_total_count = coupon_total_count;
    }
    
    /**
     * @return 返回 coupon_total_fee
     */
    public String getCoupon_total_fee()
    {
        return coupon_total_fee;
    }
    
    /**
     * @param 对coupon_total_fee进行赋值
     */
    public void setCoupon_total_fee(String coupon_total_fee)
    {
        this.coupon_total_fee = coupon_total_fee;
    }
    
    /**
     * @return 返回 refund_total_count
     */
    public String getRefund_total_count()
    {
        return refund_total_count;
    }
    
    /**
     * @param 对refund_total_count进行赋值
     */
    public void setRefund_total_count(String refund_total_count)
    {
        this.refund_total_count = refund_total_count;
    }
    
    /**
     * @return 返回 refund_total_fee
     */
    public String getRefund_total_fee()
    {
        return refund_total_fee;
    }
    
    /**
     * @param 对refund_total_fee进行赋值
     */
    public void setRefund_total_fee(String refund_total_fee)
    {
        this.refund_total_fee = refund_total_fee;
    }
    
    /**
     * @return 返回 success_total_count
     */
    public String getSuccess_total_count()
    {
        return success_total_count;
    }
    
    /**
     * @param 对success_total_count进行赋值
     */
    public void setSuccess_total_count(String success_total_count)
    {
        this.success_total_count = success_total_count;
    }
    
    /**
     * @return 返回 total_net_fee
     */
    public String getTotal_net_fee()
    {
        return total_net_fee;
    }
    
    /**
     * @param 对total_net_fee进行赋值
     */
    public void setTotal_net_fee(String total_net_fee)
    {
        this.total_net_fee = total_net_fee;
    }
    
    /**
     * @return 返回 success_total_fee
     */
    public String getSuccess_total_fee()
    {
        return success_total_fee;
    }
    
    /**
     * @param 对success_total_fee进行赋值
     */
    public void setSuccess_total_fee(String success_total_fee)
    {
        this.success_total_fee = success_total_fee;
    }
    
    private String refund_total_fee;// 总收款金额
    
    private String success_total_count;// 总收款笔数
    
    private String total_net_fee;// 交易总净额
    
    private String success_total_fee;// 总笔数
}
