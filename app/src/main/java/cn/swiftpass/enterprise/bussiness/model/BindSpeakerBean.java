package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/8/29.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(绑定的云播报音箱的基本数据类型)
 * @date 2019/8/29.14:25.
 */
public class BindSpeakerBean implements Serializable {
    public int userType;//用户类型: 3为商户，5为收银员
    public String userId;//用户id,最终用来解绑的userId
    public String userName;//用户名称,绑定的商户名称或者收银员名称
    public String account;//商户或收银员账号
    public String deviceName;//设备名称,绑定的音箱的speakerID = SN = deviceName

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

}
