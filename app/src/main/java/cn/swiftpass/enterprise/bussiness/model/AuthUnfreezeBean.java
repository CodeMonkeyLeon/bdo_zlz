package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

public class AuthUnfreezeBean implements Serializable {

    /**
     * authNo : 100570000058201812111076589656
     * requestNo : 100570000058201812111076589667
     * outRequestNo : 100570000058273062374777795
     * operationId :
     * totalFee :
     * unFreezeTime : 2018-12-11 20:00:10
     */

    private String authNo;
    private String requestNo;
    private String outRequestNo;
    private String operationId;
    private String totalFee;
    private String unFreezeTime;

    public String getAuthNo() {
        return authNo;
    }

    public void setAuthNo(String authNo) {
        this.authNo = authNo;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getOutRequestNo() {
        return outRequestNo;
    }

    public void setOutRequestNo(String outRequestNo) {
        this.outRequestNo = outRequestNo;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getUnFreezeTime() {
        return unFreezeTime;
    }

    public void setUnFreezeTime(String unFreezeTime) {
        this.unFreezeTime = unFreezeTime;
    }
}
