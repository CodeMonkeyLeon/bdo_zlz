/*
 * 文 件 名:  ErrorMsg.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-12-6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2016-12-6]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ErrorMsg implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private String message;
    
    /**
     * @return 返回 message
     */
    public String getMessage()
    {
        return message;
    }
    
    /**
     * @param 对message进行赋值
     */
    public void setMessage(String message)
    {
        this.message = message;
    }
    
    /**
     * @return 返回 content
     */
    public String getContent()
    {
        return content;
    }
    
    /**
     * @param 对content进行赋值
     */
    public void setContent(String content)
    {
        this.content = content;
    }
    
    private String content;
    
}
