package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

public class WxCard implements Serializable
{
    
    private String cardType;// 卡券类型 (groupon:团购劵；cash:代金劵；discount:折扣劵；gift:礼品劵)
    
    private String gift;// 礼品名称
    
    private long discount; // 折扣额度
    
    private long useTime;
    
    private String vcardUseTime;
    
    private String beginTimestamp;
    
    private String endTimestamp;
    
    private String type;
    
    private Integer fixedTerm;
    
    private String beginTimeStr;
    
    private String useTimeNew;
    
    private String userName;
    
    private long uId;
    
    /**
     * @return 返回 uId
     */
    public long getuId()
    {
        return uId;
    }
    
    /**
     * @param 对uId进行赋值
     */
    public void setuId(long uId)
    {
        this.uId = uId;
    }
    
    /**
     * @return 返回 userName
     */
    public String getUserName()
    {
        return userName;
    }
    
    /**
     * @param 对userName进行赋值
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    /**
     * @return 返回 useTimeNew
     */
    public String getUseTimeNew()
    {
        return useTimeNew;
    }
    
    /**
     * @param 对useTimeNew进行赋值
     */
    public void setUseTimeNew(String useTimeNew)
    {
        this.useTimeNew = useTimeNew;
    }
    
    /**
     * @return 返回 beginTimeStr
     */
    public String getBeginTimeStr()
    {
        return beginTimeStr;
    }
    
    /**
     * @param 对beginTimeStr进行赋值
     */
    public void setBeginTimeStr(String beginTimeStr)
    {
        this.beginTimeStr = beginTimeStr;
    }
    
    private String endTimeStr;
    
    /**
     * @return 返回 endTimeStr
     */
    public String getEndTimeStr()
    {
        return endTimeStr;
    }
    
    /**
     * @param 对endTimeStr进行赋值
     */
    public void setEndTimeStr(String endTimeStr)
    {
        this.endTimeStr = endTimeStr;
    }
    
    /**
     * @return 返回 fixedTerm
     */
    public Integer getFixedTerm()
    {
        return fixedTerm;
    }
    
    /**
     * @param 对fixedTerm进行赋值
     */
    public void setFixedTerm(Integer fixedTerm)
    {
        this.fixedTerm = fixedTerm;
    }
    
    /**
     * @return 返回 type
     */
    public String getType()
    {
        return type;
    }
    
    /**
     * @param 对type进行赋值
     */
    public void setType(String type)
    {
        this.type = type;
    }
    
    /**
     * @return 返回 vcardUseTime
     */
    public String getVcardUseTime()
    {
        return vcardUseTime;
    }
    
    /**
     * @return 返回 beginTimestamp
     */
    public String getBeginTimestamp()
    {
        return beginTimestamp;
    }
    
    /**
     * @param 对beginTimestamp进行赋值
     */
    public void setBeginTimestamp(String beginTimestamp)
    {
        this.beginTimestamp = beginTimestamp;
    }
    
    /**
     * @return 返回 endTimestamp
     */
    public String getEndTimestamp()
    {
        return endTimestamp;
    }
    
    /**
     * @param 对endTimestamp进行赋值
     */
    public void setEndTimestamp(String endTimestamp)
    {
        this.endTimestamp = endTimestamp;
    }
    
    /**
     * @param 对vcardUseTime进行赋值
     */
    public void setVcardUseTime(String vcardUseTime)
    {
        this.vcardUseTime = vcardUseTime;
    }
    
    /**
     * @return 返回 useTime
     */
    public long getUseTime()
    {
        return useTime;
    }
    
    /**
     * @param 对useTime进行赋值
     */
    public void setUseTime(long useTime)
    {
        this.useTime = useTime;
    }
    
    /**
     * @return 返回 cardType
     */
    public String getCardType()
    {
        return cardType;
    }
    
    /**
     * @param 对cardType进行赋值
     */
    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }
    
    /**
     * @return 返回 gift
     */
    public String getGift()
    {
        return gift;
    }
    
    /**
     * @param 对gift进行赋值
     */
    public void setGift(String gift)
    {
        this.gift = gift;
    }
    
    /**
     * @return 返回 discount
     */
    public long getDiscount()
    {
        return discount;
    }
    
    /**
     * @param 对discount进行赋值
     */
    public void setDiscount(long discount)
    {
        this.discount = discount;
    }
    
    /**
     * @return 返回 reduceCost
     */
    public long getReduceCost()
    {
        return reduceCost;
    }
    
    /**
     * @param 对reduceCost进行赋值
     */
    public void setReduceCost(long reduceCost)
    {
        this.reduceCost = reduceCost;
    }
    
    private long reduceCost;// 减免金额(分)
    
    private String cardCode;
    
    /**
     * @return 返回 cardCode
     */
    public String getCardCode()
    {
        return cardCode;
    }
    
    /**
     * @param 对cardCode进行赋值
     */
    public void setCardCode(String cardCode)
    {
        this.cardCode = cardCode;
    }
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * @return 返回 title
     */
    public String getTitle()
    {
        return title;
    }
    
    /**
     * @param 对title进行赋值
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    /**
     * @return 返回 dealDetail
     */
    public String getDealDetail()
    {
        return dealDetail;
    }
    
    /**
     * @param 对dealDetail进行赋值
     */
    public void setDealDetail(String dealDetail)
    {
        this.dealDetail = dealDetail;
    }
    
    /**
     * @return 返回 cardId
     */
    public String getCardId()
    {
        return cardId;
    }
    
    /**
     * @param 对cardId进行赋值
     */
    public void setCardId(String cardId)
    {
        this.cardId = cardId;
    }
    
    /**
     * @return 返回 mchId
     */
    public String getMchId()
    {
        return mchId;
    }
    
    /**
     * @param 对mchId进行赋值
     */
    public void setMchId(String mchId)
    {
        this.mchId = mchId;
    }
    
    /**
     * @return 返回 outTradeNo
     */
    public String getOutTradeNo()
    {
        return outTradeNo;
    }
    
    /**
     * @param 对outTradeNo进行赋值
     */
    public void setOutTradeNo(String outTradeNo)
    {
        this.outTradeNo = outTradeNo;
    }
    
    private String title;
    
    private String dealDetail;
    
    private String cardId; // 主键
    
    private String mchId;
    
    private String outTradeNo;
}
