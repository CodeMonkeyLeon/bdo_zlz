package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/4/22.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(报表折线图的基本数据类型)
 * @date 2019/4/22.16:26.
 */
public class DailyTransactionDataBean implements Serializable {
    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }

    public Long getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Long refundAmount) {
        this.refundAmount = refundAmount;
    }

    public int getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(int refundCount) {
        this.refundCount = refundCount;
    }

    public String getTransactionStartDate() {
        return transactionStartDate;
    }

    public void setTransactionStartDate(String transactionStartDate) {
        this.transactionStartDate = transactionStartDate;
    }

    public String getTransactionEndDate() {
        return transactionEndDate;
    }

    public void setTransactionEndDate(String transactionEndDate) {
        this.transactionEndDate = transactionEndDate;
    }

    public  Long transactionAmount; //交易金额
    public  int transactionCount; //交易笔数
    public  Long refundAmount; //退款金额
    public  int refundCount; //退款笔数
    public  String transactionStartDate; //开始时间
    public  String transactionEndDate; //结束时间

}
