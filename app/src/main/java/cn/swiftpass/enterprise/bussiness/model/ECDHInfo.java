package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2018/8/25.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/8/25.14:36.
 */

public class ECDHInfo implements Serializable {

    public String serPubKey;  //服务端公钥
    public String skey;       //密钥标识
}
