/*
 * 文 件 名:  Blance.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-8-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * 提现 余额显示
 * 
 * @author  he_hui
 * @version  [版本号, 2016-8-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class Blance implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private int ishaveAccount; //是否有电子账户 0是无  1是有   (没有电子账户，result=200)
    
    private String currency;// 币种CNY
    
    private String availBlance;//可用余额 单位分
    
    private String acNoBlance;//账户余额
    
    private String wDAcNo; //银行账号 
    
    private String trsType; // 交易类型
    
    private String coPatrnerJnlNo; //流水号
    
    private String trsDate; //交易日期
    
    private boolean isFlag;
    
    private String uuId;
    
    private String respurl;
    
    /**
     * @return 返回 uuId
     */
    public String getUuId()
    {
        return uuId;
    }
    
    /**
     * @param 对uuId进行赋值
     */
    public void setUuId(String uuId)
    {
        this.uuId = uuId;
    }
    
    /**
     * @return 返回 respurl
     */
    public String getRespurl()
    {
        return respurl;
    }
    
    /**
     * @param 对respurl进行赋值
     */
    public void setRespurl(String respurl)
    {
        this.respurl = respurl;
    }
    
    /**
     * @return 返回 isFlag
     */
    public boolean isFlag()
    {
        return isFlag;
    }
    
    /**
     * @param 对isFlag进行赋值
     */
    public void setFlag(boolean isFlag)
    {
        this.isFlag = isFlag;
    }
    
    /**
     * @return 返回 trsType
     */
    public String getTrsType()
    {
        return trsType;
    }
    
    /**
     * @param 对trsType进行赋值
     */
    public void setTrsType(String trsType)
    {
        this.trsType = trsType;
    }
    
    /**
     * @return 返回 coPatrnerJnlNo
     */
    public String getCoPatrnerJnlNo()
    {
        return coPatrnerJnlNo;
    }
    
    /**
     * @param 对coPatrnerJnlNo进行赋值
     */
    public void setCoPatrnerJnlNo(String coPatrnerJnlNo)
    {
        this.coPatrnerJnlNo = coPatrnerJnlNo;
    }
    
    /**
     * @return 返回 trsDate
     */
    public String getTrsDate()
    {
        return trsDate;
    }
    
    /**
     * @param 对trsDate进行赋值
     */
    public void setTrsDate(String trsDate)
    {
        this.trsDate = trsDate;
    }
    
    /**
     * @return 返回 trsTime
     */
    public String getTrsTime()
    {
        return trsTime;
    }
    
    /**
     * @param 对trsTime进行赋值
     */
    public void setTrsTime(String trsTime)
    {
        this.trsTime = trsTime;
    }
    
    private String trsTime; // 交易时间
    
    /**
     * @return 返回 amount
     */
    public String getAmount()
    {
        return amount;
    }
    
    /**
     * @param 对amount进行赋值
     */
    public void setAmount(String amount)
    {
        this.amount = amount;
    }
    
    /**
     * @return 返回 channelSeq
     */
    public String getChannelSeq()
    {
        return channelSeq;
    }
    
    /**
     * @param 对channelSeq进行赋值
     */
    public void setChannelSeq(String channelSeq)
    {
        this.channelSeq = channelSeq;
    }
    
    /**
     * @return 返回 cifName
     */
    public String getCifName()
    {
        return cifName;
    }
    
    /**
     * @param 对cifName进行赋值
     */
    public void setCifName(String cifName)
    {
        this.cifName = cifName;
    }
    
    /**
     * @return 返回 wDBankNo
     */
    public String getwDBankNo()
    {
        return wDBankNo;
    }
    
    /**
     * @param 对wDBankNo进行赋值
     */
    public void setwDBankNo(String wDBankNo)
    {
        this.wDBankNo = wDBankNo;
    }
    
    private String wDBankName; //开户名称
    
    private String amount;//提现金额
    
    private String channelSeq; //流水号
    
    private String cifName;//客户姓名
    
    private String wDBankNo;//银联号
    
    /**
     * @return 返回 ishaveAccount
     */
    public int getIshaveAccount()
    {
        return ishaveAccount;
    }
    
    /**
     * @param 对ishaveAccount进行赋值
     */
    public void setIshaveAccount(int ishaveAccount)
    {
        this.ishaveAccount = ishaveAccount;
    }
    
    /**
     * @return 返回 currency
     */
    public String getCurrency()
    {
        return currency;
    }
    
    /**
     * @param 对currency进行赋值
     */
    public void setCurrency(String currency)
    {
        this.currency = currency;
    }
    
    /**
     * @return 返回 availBlance
     */
    public String getAvailBlance()
    {
        return availBlance;
    }
    
    /**
     * @param 对availBlance进行赋值
     */
    public void setAvailBlance(String availBlance)
    {
        this.availBlance = availBlance;
    }
    
    /**
     * @return 返回 acNoBlance
     */
    public String getAcNoBlance()
    {
        return acNoBlance;
    }
    
    /**
     * @param 对acNoBlance进行赋值
     */
    public void setAcNoBlance(String acNoBlance)
    {
        this.acNoBlance = acNoBlance;
    }
    
    /**
     * @return 返回 wDAcNo
     */
    public String getwDAcNo()
    {
        return wDAcNo;
    }
    
    /**
     * @param 对wDAcNo进行赋值
     */
    public void setwDAcNo(String wDAcNo)
    {
        this.wDAcNo = wDAcNo;
    }
    
    /**
     * @return 返回 wDBankName
     */
    public String getwDBankName()
    {
        return wDBankName;
    }
    
    /**
     * @param 对wDBankName进行赋值
     */
    public void setwDBankName(String wDBankName)
    {
        this.wDBankName = wDBankName;
    }
    
}
