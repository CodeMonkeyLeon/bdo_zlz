
package cn.swiftpass.enterprise.bussiness.logica.download.inteferace;

import cn.swiftpass.enterprise.bussiness.logica.download.DownloadTask;


public interface DownloadTaskListener {
	/**
	 * @description: 开始下载前
	 */
	public void preDownload(DownloadTask task);
	
	/**
	 * @description: 下载进度更新
	 */
    public void updateProcess(DownloadTask task);
    
    /**
     * @description: 下载成功
     */
    public void downloadSuccess(DownloadTask task);
    /**
     * @description: 文件存在
     */
  //  public void downloadFileExists(DownloadTask task);
    
    /**
     * @description: 下载出错
     */
    public void downloadError(DownloadTask task, Throwable error);
    
    /**
     * @description: 下载任务真正停止
     */
    public void postDownload(DownloadTask task);
}
