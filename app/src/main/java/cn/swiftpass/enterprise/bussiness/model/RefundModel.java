package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-15
 * Time: 下午9:02
 * To change this template use File | Settings | File Templates.
 */
public class RefundModel implements Serializable{

    /**自动生成ID*/
    public Integer id;
    /**退单号*/
    public String refundNo;
    /**订单号*/
    public String orderNo;
    /**申请退单的操作员*/
    public String uId;
    /**验证退单人*/
    public Integer verifyId;
    /**退单总金额*/
    public Integer totalFee;
    /**退单金额*/
    public Integer refundMoney;
    /**添加时间*/
    public Date addTime;
    /**备注*/
    public String remark;
    /**商户id*/
    public String merchantId;
    /**申请退款状态 0.未审核 1.审核通过 2.*/
    public Integer state;

    public Date orderTime;

    public String userName;

    public int orderState;

    //////////////////////////财付通返回数据字段//////////////////////////////////
    /**财付通返回状态码*/
    public Integer retcode;
}
