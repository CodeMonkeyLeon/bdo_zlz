package cn.swiftpass.enterprise.ui.activity.otp.interfaces

/**
 * @author lizheng.zhao
 * @date 2022/12/28
 */
interface PWordCallback {
    fun pWordOnTouch(action: Int, index: Int, imp: PWordImp?)
}