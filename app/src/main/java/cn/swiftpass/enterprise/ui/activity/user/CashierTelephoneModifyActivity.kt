package cn.swiftpass.enterprise.ui.activity.user

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.bussiness.model.UserModel
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.activity.TemplateActivity
import cn.swiftpass.enterprise.ui.widget.TitleBar.OnTitleBarClickListener
import cn.swiftpass.enterprise.utils.ECDHUtils
import cn.swiftpass.enterprise.utils.SharedPreUtile


/**
 * @author lizheng.zhao
 * @date 2023/01/05
 * @description 修改手机号页面
 */
class CashierTelephoneModifyActivity : TemplateActivity(), View.OnClickListener {

    private fun getLayoutId() = R.layout.act_cashier_telephone_modify

    companion object {
        const val DATA = "DATA"
        const val REQUEST_CODE = 2345
        const val RESULT_CODE = 3345
        const val CASHIER_PHONE = "CashierTelephone"
        const val PHONE_MAX_LENGTH = 10
        const val TAG = "PhoneModify"
        const val ERROR_CODE_405 = "405"
        const val PUBLIC_KEY = "pubKey"
        const val PRIVATE_KEY = "priKey"
        const val SECRET_KEY = "secretKey"

        fun startCashierTelephoneModifyActivity(
            fromActivity: TemplateActivity?,
            data: UserModel?
        ) {
            fromActivity?.let { act ->
                data?.let { d ->
                    val intent = Intent(act, CashierTelephoneModifyActivity::class.java)
                    intent.putExtra(DATA, d)
                    act.startActivityForResult(intent, REQUEST_CODE)
                }
            }
        }
    }

    private var mUserModel: UserModel? = null

    private lateinit var mEtPhone: EditText
    private lateinit var mImgPhoneClear: ImageView
    private lateinit var mBtnConfirm: Button


    private val mTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable?) {
            if (mEtPhone.isFocused && !TextUtils.isEmpty(getInputPhone())) {
                //手机号不空
                mImgPhoneClear.visibility = View.VISIBLE
                setButtonBg(mBtnConfirm, true, 0)
            } else {
                //手机号为空
                mImgPhoneClear.visibility = View.GONE
                setButtonBg(mBtnConfirm, false, 0)
            }
        }
    }


    private val mTextLengthFilter = InputFilter.LengthFilter(PHONE_MAX_LENGTH)
//    private val mTextContentFilter =
//        InputFilter { source, start, end, dest, dstart, dend ->
//            val addStr = source.toString()
//            Log.i(TAG, "source : $source")
//            Log.i(TAG, "addStr : $addStr")
//            when (addStr) {
//                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" -> source!!
//                else -> ""
//            }
//        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        intent?.let { data ->
            mUserModel = data.getSerializableExtra(DATA) as UserModel
            initView()
        }
    }


    private fun initView() {
        mEtPhone = findViewById(R.id.id_et_phone_modify)
        mImgPhoneClear = findViewById(R.id.id_iv_phone_modify)
        mBtnConfirm = findViewById(R.id.id_btn_submit)


        mEtPhone.addTextChangedListener(mTextWatcher)
        mEtPhone.filters = arrayOf(mTextLengthFilter)

        mImgPhoneClear.setOnClickListener(this)
        mBtnConfirm.setOnClickListener(this)

        mUserModel?.let { data ->
            val phone = data.mobileNumber
            mEtPhone.setText(phone)
            if (TextUtils.isEmpty(phone)) {
                //手机号为空, 隐藏清空按钮
                mImgPhoneClear.visibility = View.GONE
                setButtonBg(mBtnConfirm, false, 0)
            } else {
                //手机号不空, 显示清空按钮
                mImgPhoneClear.visibility = View.VISIBLE
                setButtonBg(mBtnConfirm, true, 0)
            }
        }
    }


    override fun onClick(view: View?) {
        view?.let { v ->
            when (v.id) {
                R.id.id_iv_phone_modify -> {
                    //点击清除按钮
                    mEtPhone.setText("")
                }
                R.id.id_btn_submit -> {
                    //点击确认按钮
                    val phone = getInputPhone()
                    if (!TextUtils.isEmpty(phone)) {
                        //输入的不空
                        mUserModel?.let {
                            it.mobileNumber = phone
                            submitData(it)
                        }
                    } else {
                        //输入为空
                        toastDialog(
                            this@CashierTelephoneModifyActivity,
                            R.string.tx_surname_notnull,
                            null
                        )
                    }
                }
                else -> {

                }
            }
        }
    }


    private fun submitData(data: UserModel) {
        UserManager.cashierAdd(data, false, object : UINotifyListener<Boolean>() {
            override fun onPreExecute() {
                super.onPreExecute()
                showLoading(false, getString(R.string.show_save_loading))
            }

            override fun onError(error: Any?) {
                super.onError(error)
                dismissLoading()
                if (checkSession()) {
                    return
                }
                error?.let { e ->
                    runOnUiThread {
                        if (e.toString().startsWith(ERROR_CODE_405)) {
                            //需要交换密钥
                            ecdhKeyExchange(data)
                        } else {
                            toastDialog(
                                this@CashierTelephoneModifyActivity,
                                e.toString(),
                                null
                            )
                        }
                    }
                }
            }


            override fun onSucceed(result: Boolean) {
                super.onSucceed(result)
                dismissLoading()
                if (result) {
                    mUserModel?.let { user ->
                        val data = Intent()
                        data.putExtra(CASHIER_PHONE, user.mobileNumber)
                        setResult(RESULT_CODE, data)
                        finish()
                    }
                }
            }
        })
    }


    private fun ecdhKeyExchange(data: UserModel) {
        try {
            ECDHUtils.getInstance().appPubKey
        } catch (e: Exception) {
            Log.e(TAG, Log.getStackTraceString(e))
        }
        val publicKey = SharedPreUtile.readProduct(PUBLIC_KEY).toString()
        val privateKey = SharedPreUtile.readProduct(PRIVATE_KEY).toString()


        LocalAccountManager.getInstance().ECDHKeyExchange(publicKey,
            object : UINotifyListener<ECDHInfo>() {
                override fun onError(error: Any?) {
                    super.onError(error)
                }

                override fun onSucceed(result: ECDHInfo?) {
                    super.onSucceed(result)
                    result?.let {
                        try {
                            val secretKey = ECDHUtils.getInstance()
                                .ecdhGetShareKey(MainApplication.serPubKey, privateKey)
                            SharedPreUtile.saveObject(secretKey, SECRET_KEY)
                        } catch (e: Exception) {
                            Log.e(TAG, Log.getStackTraceString(e))
                        }
                        submitData(data)
                    }
                }
            })
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
        }
        return super.onKeyDown(keyCode, event)
    }


    private fun getInputPhone() = mEtPhone.text.toString()

    override fun setButtonBg(b: Button?, enable: Boolean, res: Int) {
        super.setButtonBg(b, enable, res)
        if (enable) {
            b!!.isEnabled = true
            b.setTextColor(resources.getColor(R.color.white))
            if (res > 0) {
                b.setText(res)
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape)
        } else {
            b!!.isEnabled = false
            if (res > 0) {
                b.setText(res)
            }
            b.setBackgroundResource(R.drawable.btn_press_shape)
            b.setTextColor(resources.getColor(R.color.bt_enable))
            b.background.alpha = 102
        }
    }


    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setLeftButtonVisible(true)
        titleBar.setTitle(R.string.string_phone)
        titleBar.setOnTitleBarClickListener(object : OnTitleBarClickListener {
            override fun onRightLayClick() {}
            override fun onRightButtonClick() {}
            override fun onRightButLayClick() {}
            override fun onLeftButtonClick() {
                finish()
            }
        })
    }

}