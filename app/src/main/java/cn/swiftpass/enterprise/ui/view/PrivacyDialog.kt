package cn.swiftpass.enterprise.ui.view

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.TypefaceSpan
import android.util.DisplayMetrics
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import cn.swiftpass.enterprise.interfaces.OnPrivacyDialogClickListener
import cn.swiftpass.enterprise.intl.R


/**
 * 隐私政策弹框
 */
class PrivacyDialog private constructor(val mContext: Context) :
    Dialog(mContext) {


    private lateinit var mTvTitle: TextView
    private lateinit var mMyScrollTextView: MyScrollTextView
    private lateinit var mTvAgree: TextView
    private lateinit var mBtnAgree: TextView
    private lateinit var mImgClose: ImageView
    private lateinit var mImgChecked: ImageView

    private var mOnPrivacyDialogClickListener: OnPrivacyDialogClickListener? = null

    private fun setOnPrivacyDialogClickListener(listener: OnPrivacyDialogClickListener?) {
        mOnPrivacyDialogClickListener = listener
    }

    private var mChecked = false


    //字体样式
    private val mBoldTypeface by lazy {
        Typeface.createFromAsset(mContext.assets, "Nunito-ExtraBold.ttf")
    }
    private val mNormalTypeface by lazy {
        Typeface.createFromAsset(mContext.assets, "Nunito-Regular.ttf")
    }


    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_privacy)

        mTvTitle = findViewById(R.id.id_tv_title)
        mMyScrollTextView = findViewById(R.id.id_scroll_text_view)
        mTvAgree = findViewById(R.id.id_tv_check)
        mBtnAgree = findViewById(R.id.id_btn_agree)
        mImgClose = findViewById(R.id.id_img_close)
        mImgChecked = findViewById(R.id.id_img_check)

        mTvTitle.typeface = mBoldTypeface
//        mMyScrollTextView.setTypeface(mNormalTypeface)
        mTvAgree.typeface = mNormalTypeface
        mBtnAgree.typeface = mBoldTypeface

        setCancelable(false)
        setWindows()
        initEvent()
        initPrivacy()
    }


    private fun initPrivacy() {
        val text = mContext.getString(R.string.string_privacy_agree)
        val targetText = mContext.getString(R.string.privacy_terms)

        val st = text.indexOf(targetText)
        val en = st + targetText.length

        val spannableString = SpannableString(text)

        //设置字体
        val normal = object : TypefaceSpan(mNormalTypeface) {}
        spannableString.setSpan(normal, 0, text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        val bold = object : TypefaceSpan(mBoldTypeface) {}
        spannableString.setSpan(bold, st, en, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        //点击事件
        val clickSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                mOnPrivacyDialogClickListener?.run {
                    onPrivacyClick()
                }
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.color = mContext.resources.getColor(R.color.color_scroll_text_agree_black)
                ds.isUnderlineText = false
            }
        }
        spannableString.setSpan(clickSpan, st, en, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        //赋值
        mTvAgree.text = spannableString
        mTvAgree.movementMethod = LinkMovementMethod.getInstance()


    }


    private fun initEvent() {
        //点击关闭
        mImgClose.setOnClickListener {
            mOnPrivacyDialogClickListener?.run {
                onCloseClick()
            }
        }

        //点击同意
        mBtnAgree.setOnClickListener {
            mOnPrivacyDialogClickListener?.run {
                if (mChecked) {
                    onAgreeClick()
                    dismiss()
                }
            }
        }

        //点击勾选
        mImgChecked.setOnClickListener {
            onCheck()
        }
    }


    private fun onCheck() {
        mChecked = !mChecked
        if (mChecked) {
            mImgChecked.setImageResource(R.drawable.icon_privacy_check_select)
            mBtnAgree.setBackgroundResource(R.drawable.shape_privacy_agree_enable)
        } else {
            mImgChecked.setImageResource(R.drawable.icon_privacy_check_default)
            mBtnAgree.setBackgroundResource(R.drawable.shape_privacy_agree_disable)
        }
    }


    private fun setWindows() {
        window?.run {
            // 获取屏幕宽度和高度
            val displayMetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            val screenWidth = displayMetrics.widthPixels
            val screenHeight = displayMetrics.heightPixels

            // 计算 Dialog 宽度和高度
            val dialogWidth = (screenWidth * 0.9).toInt()
            val dialogHeight = (screenHeight * 0.9).toInt()

            // 设置 Dialog 的宽度和高度
            setLayout(dialogWidth, dialogHeight)

            //设置背景
            setBackgroundDrawable(mContext.resources.getDrawable(R.drawable.bg_privacy_dialog))
        }
    }


    class Builder {
        private var context: Context? = null
        private var listener: OnPrivacyDialogClickListener? = null


        fun setContext(context: Context?): Builder {
            context?.let {
                this.context = it
            }
            return this
        }

        fun setClickListener(listener: OnPrivacyDialogClickListener?): Builder {
            this.listener = listener
            return this
        }


        fun build(): PrivacyDialog? {
            context?.run {
                val privacyDialog = PrivacyDialog(this)
                privacyDialog.setOnPrivacyDialogClickListener(listener)
                return privacyDialog
            }
            return null
        }

    }


}