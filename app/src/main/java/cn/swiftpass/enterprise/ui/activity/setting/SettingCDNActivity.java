package cn.swiftpass.enterprise.ui.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

/**
 * Created by aijingya on 2018/2/7.
 *
 * @Package cn.swiftpass.enterprise.ui.activity.setting
 * @Description: ${TODO}(从登陆页面进入的网络连接优化功能的页面)
 * @date 2018/2/7.16:44.
 */

public class SettingCDNActivity extends TemplateActivity {

    ImageView iv_select_cdn;

    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting);
        initView();
    }

    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.setting_network);
    }

    public void initView(){
        iv_select_cdn = getViewById(R.id.iv_select_cdn);
        //默认是开启的状态，但是可控制开启或者关闭
        String CDN_status = PreferenceUtil.getString("CDN", "open");
        if(CDN_status.equals("open")){
            iv_select_cdn.setSelected(true);
        }else{
            iv_select_cdn.setSelected(false);
        }

        iv_select_cdn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_select_cdn.setSelected(iv_select_cdn.isSelected() ? false : true);
                if(iv_select_cdn.isSelected()){
                    PreferenceUtil.commitString("CDN", "open");
                }else{
                    PreferenceUtil.commitString("CDN", "close");
                }
            }
        });
    }
}
