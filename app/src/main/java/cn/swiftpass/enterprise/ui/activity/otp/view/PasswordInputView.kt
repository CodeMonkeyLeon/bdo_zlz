package cn.swiftpass.enterprise.ui.activity.otp.view

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Rect
import android.os.Handler
import android.text.InputType
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.ViewGroup
import android.view.inputmethod.BaseInputConnection
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.view.inputmethod.InputMethodManager
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.activity.otp.interfaces.PWordCallback
import cn.swiftpass.enterprise.ui.activity.otp.interfaces.PWordImp
import cn.swiftpass.enterprise.ui.activity.otp.interfaces.PasswordInputImp
import cn.swiftpass.enterprise.utils.Utils

/**
 * @author lizheng.zhao
 * @date 2022/12/28
 */
class PasswordInputView : ViewGroup, PasswordInputImp {


    companion object {
        const val TAG = "PasswordInputView"
        const val TIME_DELAY = 1800L
    }

    private var mContext: Context

    /**
     * 密码框个数
     */
    private var mCount: Int = 0

    /**
     * 记录输入的密码
     */
    private var mPassword = StringBuilder()

    /**
     * 是否显示密文
     */
    private var mIsCipher: Boolean = false

    /**
     * 当前焦点位置
     */
    private var mCurrentCursorPosition = 0

    //密码框间隔
    private var mSplitViewMargin = 0

    //密码框的高宽
    private var mSplitViewW = 0
    private var mSplitViewH = 0


    //整个view的左右间距
    private var mViewGroupMargin = 0
    private var mViewGroupW = 0
    private var mViewGroupH = 0

    constructor(context: Context) : super(context) {
        mContext = context
        initConfiguration()
        initView()
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        mContext = context
        initConfiguration(mContext.obtainStyledAttributes(attributeSet, R.styleable.MySplitView))
        initView(attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attributeSet,
        defStyleAttr
    ) {
        mContext = context
        initConfiguration(mContext.obtainStyledAttributes(attributeSet, R.styleable.MySplitView))
        initView(attributeSet)
    }


    private fun initConfiguration(
        array: TypedArray? = null,
        pViewCount: Int = 6,
        splitViewMargin: Int = 5,
        passwordInputViewMargin: Int = 10
    ) {
        if (array == null) {
            mCount = pViewCount
            mSplitViewMargin = Utils.dp2px(splitViewMargin)
            mViewGroupMargin = Utils.dp2px(passwordInputViewMargin)
        } else {
            mCount = array.getInt(R.styleable.MySplitView_pViewCount, pViewCount)
            mSplitViewMargin = Utils.dp2px(
                array.getInt(R.styleable.MySplitView_passwordSplitMargin, splitViewMargin)
            )
            mViewGroupMargin = Utils.dp2px(
                array.getInt(
                    R.styleable.MySplitView_passwordInputViewMargin,
                    passwordInputViewMargin
                )
            )
        }
    }

    private fun initView(attributeSet: AttributeSet? = null) {
        for (i in 0 until mCount) {
            val pWordView = PWordView(mContext, attributeSet, 0, i)
            pWordView.setOnPWordCallback(object : PWordCallback {
                override fun pWordOnTouch(action: Int, index: Int, imp: PWordImp?) {
                    imp?.let { view ->
                        val currentImp = getChildAt(mCurrentCursorPosition) as PWordImp
                        when (action) {
                            MotionEvent.ACTION_DOWN -> {
                                //获取焦点
                                reqFocus()
                                when (mCurrentCursorPosition) {
                                    in 0 until 5 -> {
                                        currentImp.showCursor()
                                    }
                                    5 -> {
                                        if (TextUtils.isEmpty(currentImp.getText())) {
                                            currentImp.showCursor()
                                        } else {

                                        }
                                    }
                                    else -> {}
                                }
                            }
                            MotionEvent.ACTION_UP -> {
                                //展示键盘
                                if (mCurrentCursorPosition == 0) {
                                    //报错清空后，弹起键盘时，边框变蓝色
                                    setFrameColor(mContext.resources.getColor(R.color.color_pword_frame_blue))
                                }
                                showKeyboard()
                            }
                        }
                    }
                }
            })
            addView(pWordView)
        }
    }

    /**
     * 添加密码
     */
    private fun addPasswordText(word: String?) {
        if (mPassword.length < childCount) {
            mPassword.append(word)
            Log.i(TAG, "输入的密码: $mPassword")
        } else if (mPassword.length == childCount) {
            mPassword.replace(mPassword.length - 1, mPassword.length, word)
            Log.i(TAG, "输入的密码: $mPassword")
        }
    }

    /**
     * 删除密码
     */
    private fun deletePasswordText() {
        if (mPassword.isNotEmpty()) {
            mPassword.deleteAt(mPassword.lastIndex)
            Log.i(TAG, "输入的密码: $mPassword")
        }
    }

    /**
     * 该View是否可编辑
     */
    override fun onCheckIsTextEditor() = true


    /**
     * 监听是否失去焦点
     */
    override fun onFocusChanged(gainFocus: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect)
        if (gainFocus) {
            //获取到焦点
        } else {
            //失去焦点
            (getChildAt(mCurrentCursorPosition) as PWordImp).hideCursor()
            hideKeyboard()
        }
    }


    val showDelay: (Int, PWordImp) -> Unit = { i, imp ->
        Handler().postDelayed({
            if (imp.getIndex() == 5) {
                if (!TextUtils.isEmpty(imp.getText())) {
                    imp.inputText(PWordImp.CIPHER)
                }
            } else {
                if (imp.getIndex() < mCurrentCursorPosition) {
                    imp.inputText(PWordImp.CIPHER)
                }
            }
        }, TIME_DELAY)
    }


    /**
     * 监听键盘输入文字
     */
    override fun onCreateInputConnection(outAttrs: EditorInfo?): InputConnection {
        outAttrs?.let {
            it.inputType = InputType.TYPE_CLASS_NUMBER
            it.imeOptions = EditorInfo.IME_FLAG_NO_EXTRACT_UI
        }
        return object : BaseInputConnection(this, true) {

            override fun commitText(text: CharSequence?, newCursorPosition: Int): Boolean {
                when (val value = text.toString()) {
                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" -> {
                        val currentImp = getChildAt(mCurrentCursorPosition) as PWordImp
                        currentImp.hideCursor()
                        currentImp.inputText(value)
                        if (mIsCipher) {
                            //密文
                            if (mCurrentCursorPosition > 0) {
                                //下一个输入时，上一个立马变密文
                                val preImp = getChildAt(mCurrentCursorPosition - 1) as PWordImp
                                preImp.inputText(PWordImp.CIPHER)
                            }
                            //首先展示明文, 2s后展示密文
                            showDelay.invoke(currentImp.getIndex(), currentImp)
                        }
                        addPasswordText(value)
                        when (mCurrentCursorPosition) {
                            in 0 until 5 -> {
                                mCurrentCursorPosition++
                                val nextImp = getChildAt(mCurrentCursorPosition) as PWordImp
                                nextImp.showCursor()
                            }
                            5 -> {

                            }
                        }

                    }
                    "/n" -> {
                        //回车
                    }
                }
                return true
            }

            override fun deleteSurroundingText(beforeLength: Int, afterLength: Int): Boolean {
                val currentImp = getChildAt(mCurrentCursorPosition) as PWordImp
                when (mCurrentCursorPosition) {
                    0 -> {
                        currentImp.inputText("")
                        deletePasswordText()
                    }
                    in 1..5 -> {
                        val text = currentImp.getText()
                        if (TextUtils.isEmpty(text)) {
                            currentImp.hideCursor()
                            mCurrentCursorPosition--
                            val preImp = getChildAt(mCurrentCursorPosition) as PWordImp
                            preImp.inputText("")
                            deletePasswordText()
                            preImp.showCursor()
                            if (mCurrentCursorPosition == 0) {
                                setFrameColor(mContext.resources.getColor(R.color.color_pword_frame_blue))
                            }
                        } else {
                            currentImp.inputText("")
                            deletePasswordText()
                            currentImp.showCursor()
                        }
                    }
                }
                return super.deleteSurroundingText(beforeLength, afterLength)
            }

            //只对系统自带键盘有效，其他软件输入法，比如搜狗输入法，输入值时，此方法有时不会调用
            //但有时也会调用，所以此处和commitText(), deleteSurroundingText()中都需要处理输入值
            override fun sendKeyEvent(event: KeyEvent?): Boolean {
                event?.let { e ->
                    if (e.action == KeyEvent.ACTION_UP) {
                        when (e.keyCode) {
                            KeyEvent.KEYCODE_ENTER -> {
                                //回车
                                commitText("/n", 0)
                            }
                            KeyEvent.KEYCODE_DEL -> {
                                //删除
                                deleteSurroundingText(1, 0)
                            }
                            KeyEvent.KEYCODE_0 -> {
                                commitText("0", 0)
                            }
                            KeyEvent.KEYCODE_1 -> {
                                commitText("1", 0)
                            }
                            KeyEvent.KEYCODE_2 -> {
                                commitText("2", 0)
                            }
                            KeyEvent.KEYCODE_3 -> {
                                commitText("3", 0)
                            }
                            KeyEvent.KEYCODE_4 -> {
                                commitText("4", 0)
                            }
                            KeyEvent.KEYCODE_5 -> {
                                commitText("5", 0)
                            }
                            KeyEvent.KEYCODE_6 -> {
                                commitText("6", 0)
                            }
                            KeyEvent.KEYCODE_7 -> {
                                commitText("7", 0)
                            }
                            KeyEvent.KEYCODE_8 -> {
                                commitText("8", 0)
                            }
                            KeyEvent.KEYCODE_9 -> {
                                commitText("9", 0)
                            }
                            else -> {

                            }
                        }
                    }
                }
                return true
            }
        }
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        mViewGroupW = MeasureSpec.getSize(widthMeasureSpec)
        mViewGroupH = MeasureSpec.getSize(heightMeasureSpec)

        //设置本ViewGroup的宽高
        setMeasuredDimension(mViewGroupW, mViewGroupH)

        //设置子View的高宽
        mSplitViewW =
            (mViewGroupW - 2 * mViewGroupMargin - (childCount - 1) * mSplitViewMargin) / childCount
//        mSplitViewH = (mViewGroupH*0.98).toInt()
        mSplitViewH = (mSplitViewW * 60 / 48.0).toInt()

        for (i in 0 until childCount) {
            val pWordView = getChildAt(i)
            pWordView.measure(mSplitViewW, mSplitViewH)
        }
    }


    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        var startLeft = mViewGroupMargin
        val top = (mViewGroupH - mSplitViewH) / 2
        for (i in 0 until childCount) {
            val splitView = getChildAt(i)
            splitView.layout(
                startLeft,
                top,
                startLeft + splitView.measuredWidth,
                top + splitView.measuredHeight
            )
            startLeft += splitView.measuredWidth + mSplitViewMargin
        }
    }

    /**
     * 获取焦点
     */
    private fun reqFocus() {
        isFocusableInTouchMode = true //important
        isFocusable = true
        requestFocus()
    }

    /**
     * 释放焦点
     */
    private fun releaseFocus() {
        isFocusableInTouchMode = false
        isFocusable = false
//        this.clearFocus()
    }


    /**
     * 调起软键盘
     */
    fun showKeyboard() {
        try {
            val imm =
                mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(this, InputMethodManager.RESULT_UNCHANGED_SHOWN)
            imm.restartInput(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 隐藏软键盘
     */
    private fun hideKeyboard() {
        try {
            val imm = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(this.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    /**
     * 设置密码框边框颜色
     */
    override fun setFrameColor(color: Int) {
        for (i in 0 until childCount) {
            (getChildAt(i) as PWordImp).setFrameColor(color)
        }
    }

    /**
     * 是否显示密文
     */
    override fun setCipher(isCipher: Boolean) {
        mIsCipher = isCipher
        for (i in mPassword.indices) {
            val word = if (isCipher) PWordImp.CIPHER else mPassword[i]
            (getChildAt(i) as PWordImp).inputText(word.toString())
        }
    }

    /**
     * 输入内容
     */
    override fun getInputPassword() = mPassword.toString()


    override fun clearInput() {
        for (i in mPassword.indices) {
            (getChildAt(i) as PWordImp).inputText("")
        }
        if (mPassword.length < 6) {
            (getChildAt(mPassword.length) as PWordImp).hideCursor()
        }
        mPassword.clear()
        mCurrentCursorPosition = 0
    }
}