package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.PreAuthDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;

public class PreAutFinishAdapter extends BaseRecycleAdapter<PreAuthDetailsBean> {


    private List<PreAuthDetailsBean> mList=new ArrayList<>();
    private PreAuthDetailsBean mPreAuthDetailsBean;
    private int activityType;
    private Context mContext;

    public PreAutFinishAdapter(Context context,List<PreAuthDetailsBean> datas,int showType) {
        super(datas);
        mList=datas;
        activityType=showType;
        mContext=context;
    }

    @Override
    protected void bindData(BaseViewHolder holder, int position) {
        mPreAuthDetailsBean=mList.get(position);

        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        TextView tv_title=(TextView) holder.getView(R.id.tv_title);
        final TextView tv_tips=(TextView) holder.getView(R.id.tv_tips);
        TextView tv_content=(TextView)holder.getView(R.id.tv_content);

        String title=mPreAuthDetailsBean.getTitle()+"";
        String title_msg=mPreAuthDetailsBean.getTitle_msg()+"";
        String msg=mPreAuthDetailsBean.getMsg()+"";

        switch (activityType){
            case 1:
                if (position==0){
                    tv_content.setVisibility(View.GONE);
                    tv_tips.setVisibility(View.VISIBLE);
                    tv_title.setLayoutParams(params);
                    tv_tips.setTextColor(Color.parseColor("#23BF3F"));
                    if (!TextUtils.isEmpty(title)){
                        tv_title.setText(title);
                    }
                    if (!TextUtils.isEmpty(msg)) {
                        tv_tips.setText(msg);
                    }
                }else {
                    tv_content.setVisibility(View.VISIBLE);
                    tv_tips.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(title)){
                        tv_title.setText(title);
                    }
                    if (!TextUtils.isEmpty(title_msg)){
                        tv_content.setText(title_msg);
                    }
                }
                break;
            case 2:
                tv_content.setVisibility(View.VISIBLE);
                tv_tips.setVisibility(View.GONE);
                tv_title.setText(title);
                tv_content.setText(title_msg);
                break;
            case 3:
                if (position==0){
                    tv_content.setVisibility(View.GONE);
                    tv_tips.setVisibility(View.VISIBLE);
                    tv_title.setLayoutParams(params);
                    tv_title.setText(title);
                    tv_tips.setTextColor(Color.parseColor("#23BF3F"));
                    tv_tips.setText(msg);
                }else {
                    tv_content.setVisibility(View.VISIBLE);
                    tv_tips.setVisibility(View.GONE);
                    tv_title.setText(title);
                    tv_content.setText(title_msg);
                }
                break;
            case 4:
                tv_content.setVisibility(View.VISIBLE);
                tv_tips.setVisibility(View.GONE);
                tv_title.setText(title);
                tv_tips.setText(msg);
                break;
            case 5:
                if (position==0){
                    tv_content.setVisibility(View.GONE);
                    tv_tips.setVisibility(View.VISIBLE);
                    tv_title.setLayoutParams(params);
                    tv_tips.setTextColor(Color.parseColor("#23BF3F"));
                    if (!TextUtils.isEmpty(title)){
                        tv_title.setText(title);
                    }
                    if (!TextUtils.isEmpty(msg)) {
                        tv_tips.setText(msg);
                    }
                }else {
                    tv_content.setVisibility(View.VISIBLE);
                    tv_tips.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(title)){
                        tv_title.setText(title);
                    }
                    if (!TextUtils.isEmpty(title_msg)){
                        tv_content.setText(title_msg);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_preauth_content;
    }
}
