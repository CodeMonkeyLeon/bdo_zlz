package cn.swiftpass.enterprise.ui.activity.otp

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener
import cn.swiftpass.enterprise.bussiness.model.RequestResult
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.net.ApiConstant
import cn.swiftpass.enterprise.io.net.NetHelper
import cn.swiftpass.enterprise.ui.activity.otp.entity.OtpEntity
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.StringUtil
import cn.swiftpass.enterprise.utils.ToastHelper
import org.json.JSONObject

/**
 * @author lizheng.zhao
 * @date 2023/01/17
 */
object OtpManager {


    const val ERROR_MSG = "message"
    const val ERROR_CODE = "code"
    const val RESULT_CODE = "result"
    const val SUCCESS = "success"
    const val PARAM_SPLIT = "###"
    const val EXTERNAL_PARAM = "externalParam"
    const val OTP_ERROR_CODE_VERIFY_CODE_ERROR = 1000409315 //验证码错误
    const val OTP_ERROR_CODE_DEVICE_EXCEED_BINDING_LIMIT = 1000409309  //设备超过绑定上限
    const val OTP_ERROR_CODE_OTP_EXPIRED = 1000409310 //OTP过期标识
    const val OTP_ERROR_CODE_SEND_COUNT_EXCEED_LIMIT = 1000409312  //发送次数到达上限
    const val OTP_ERROR_CODE_VERIFY_CODE_COUNT_EXCEED_LIMIT = 1000409313 //验证码错误次数达到上限
    const val OTP_RESEND = 1000409321  //重发otp


    /**
     * 发送otp
     */
    fun sendOrCheckOtp(
        otp: String = "",
        isCheckOtp: Boolean = true,
        loginSKey: String? = "",
        loginSauthId: String? = "",
        listener: UINotifyListener<OtpEntity>
    ) {
        ThreadHelper.executeWithCallback(
            object : Executable<OtpEntity>() {
                override fun execute(): OtpEntity? {
                    try {
                        val spayRs = System.currentTimeMillis()
                        val json = JSONObject()
                        json.put("client", MainApplication.CLIENT)
                        if (isCheckOtp) {
                            json.put("otp", otp)
                        }
                        if (!StringUtil.isEmptyOrNull(MainApplication.newSignKey)) {
                            json.put("spayRs", spayRs)
                            json.put(
                                "nns", SignUtil.getInstance().createSign(
                                    JsonUtil.jsonToMap(json.toString()), MainApplication.newSignKey
                                )
                            )
                        }
                        val url =
                            if (isCheckOtp) ApiConstant.BASE_URL_PORT + "spay/user/checkOtp" else ApiConstant.BASE_URL_PORT + "spay/user/sendOtp"
                        val result = NetHelper.httpsPost(
                            url,
                            json.toString(),
                            spayRs.toString(),
                            null,
                            loginSKey,
                            loginSauthId
                        )
                        if (!result.hasError()) {
                            //{"result":"200","message":"success","code":""}
                            if (result.data != null) {
                                val errorCode = result.data.getString(ERROR_CODE)
                                val errorMsg = result.data.getString(ERROR_MSG)
                                if (TextUtils.isEmpty(errorCode)) {
                                    val resultCode = result.data.getString(RESULT_CODE).toInt()
                                    if (resultCode == 200 && TextUtils.equals(errorMsg, SUCCESS)) {
                                        //发送成功
                                        return OtpEntity(true)
                                    } else {
                                        listener.onError(errorMsg)
                                    }
                                } else {
                                    when (errorCode.toInt()) {
                                        OTP_RESEND -> {
                                            //重发OTP
                                            //{"result":"200","message":"1000409321","code":"1000409321","externalParam":["3"]}
                                            var remainCount = ""
                                            try {
                                                remainCount =
                                                    result.data.getJSONArray(EXTERNAL_PARAM)
                                                        .getString(0)
                                            } catch (e: Exception) {

                                            }
                                            var message = ""
                                            if (!TextUtils.isEmpty(remainCount)) {
                                                message =
                                                    ToastHelper.toStr(R.string.string_new_send_remain_count)
                                                        .replace(PARAM_SPLIT, remainCount)
                                                if (TextUtils.equals(remainCount, "0")
                                                    || TextUtils.equals(remainCount, "1")
                                                ) {
                                                    message = message.replace("attempts", "attempt")
                                                }
                                            }
                                            return OtpEntity(true, message, remainCount)
                                        }
                                        OTP_ERROR_CODE_VERIFY_CODE_ERROR -> {
                                            //验证码错误
                                            var remainCount = ""
                                            try {
                                                remainCount =
                                                    result.data.getJSONArray(EXTERNAL_PARAM)
                                                        .getString(0)
                                            } catch (e: Exception) {
                                            }
                                            val errorM = if (TextUtils.isEmpty(remainCount)) {
                                                errorMsg
                                            } else {
                                                var m =
                                                    ToastHelper.toStr(R.string.string_invalid_remain_count)
                                                        .replace(
                                                            PARAM_SPLIT, remainCount
                                                        )
                                                if (TextUtils.equals(remainCount, "0")
                                                    || TextUtils.equals(remainCount, "1")
                                                ) {
                                                    m = m.replace("attempts", "attempt")
                                                }
                                                m
                                            }
                                            listener.onError("$OTP_ERROR_CODE_VERIFY_CODE_ERROR${errorM}")
                                        }
                                        OTP_ERROR_CODE_DEVICE_EXCEED_BINDING_LIMIT -> {
                                            //设备超过绑定上限
                                            listener.onError(
                                                "$OTP_ERROR_CODE_DEVICE_EXCEED_BINDING_LIMIT${errorMsg}"
                                            )
                                        }
                                        OTP_ERROR_CODE_SEND_COUNT_EXCEED_LIMIT -> {
                                            //发送次数到达上限
                                            listener.onError(
                                                "$OTP_ERROR_CODE_SEND_COUNT_EXCEED_LIMIT${errorMsg}"
                                            )
                                        }
                                        OTP_ERROR_CODE_VERIFY_CODE_COUNT_EXCEED_LIMIT -> {
                                            //验证码错误次数达到上限
                                            listener.onError(
                                                "$OTP_ERROR_CODE_VERIFY_CODE_COUNT_EXCEED_LIMIT${errorMsg}"
                                            )
                                        }
                                        OTP_ERROR_CODE_OTP_EXPIRED -> {
                                            //验证码过期
                                            listener.onError(
                                                "$OTP_ERROR_CODE_OTP_EXPIRED${errorMsg}"
                                            )
                                        }
                                        else -> {
                                            listener.onError(errorMsg)
                                        }
                                    }
                                }
                            }
                        } else {
                            when (result.resultCode) {
                                RequestResult.RESULT_BAD_NETWORK -> {
                                    listener.onError(ToastHelper.toStr(R.string.show_net_bad))
                                    listener.onError(ToastHelper.toStr(R.string.show_net_timeout))
                                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail))
                                }
                                RequestResult.RESULT_TIMEOUT_ERROR -> {
                                    listener.onError(ToastHelper.toStr(R.string.show_net_timeout))
                                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail))
                                }
                                RequestResult.RESULT_READING_ERROR -> listener.onError(
                                    ToastHelper.toStr(
                                        R.string.show_net_server_fail
                                    )
                                )
                            }
                        }
                    } catch (e: Exception) {
                        listener.onError(ToastHelper.toStr(R.string.show_net_server_fail))
                    }
                    return null
                }
            }, listener
        )
    }
}