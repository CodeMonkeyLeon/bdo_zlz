package cn.swiftpass.enterprise.ui.activity.otp.entity

data class OtpEntity(
    val isSuccess: Boolean = false,
    val message: String? = "",
    val remainCount: String? = ""
) : java.io.Serializable