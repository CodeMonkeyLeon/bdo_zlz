package cn.swiftpass.enterprise.ui.activity.otp.act

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.activity.TemplateActivity
import cn.swiftpass.enterprise.ui.activity.otp.OtpManager
import cn.swiftpass.enterprise.ui.activity.otp.entity.OtpEntity
import cn.swiftpass.enterprise.ui.activity.otp.interfaces.PasswordInputImp
import cn.swiftpass.enterprise.ui.activity.otp.view.PasswordInputView
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.KotlinUtils
import cn.swiftpass.enterprise.utils.PreferenceUtil
import cn.swiftpass.enterprise.utils.ToastHelper


/**
 * @author lizheng.zhao
 * @date 2023/01/05
 * @description otp页面
 */
class OTPActivity : TemplateActivity(), View.OnClickListener {

    private fun getLayoutId() = R.layout.act_otp


    private lateinit var mTvTitle: TextView
    private lateinit var mTvSubTitle: TextView
    private lateinit var mPasswordInputView: PasswordInputView
    private lateinit var mImageEye: ImageView
    private lateinit var mTvError: TextView
    private lateinit var mBtnResendOtp: TextView
    private lateinit var mTvStaticTips: TextView
    private lateinit var mBtnSubmit: Button

    private lateinit var mPInputImp: PasswordInputImp
    private var mIsOpenEye = false


    private var mMobileNumber: String? = ""
    private var mLoginSkey: String? = ""
    private var mLoginSauthId: String? = ""
    private var mUserName: String? = ""


    //字体样式
    private val mBoldTypeface by lazy {
        Typeface.createFromAsset(assets, "Nunito-ExtraBold.ttf")
    }
    private val mNormalTypeface by lazy {
        Typeface.createFromAsset(assets, "Nunito-Regular.ttf")
    }


    private var mTimeCount = TIME_COUNT
    private val mTimeHandler = Handler()
    private val mTimeRunnable = object : Runnable {
        override fun run() {
            if (mTimeCount-- >= 0) {
                mBtnResendOtp.isEnabled = false
                mBtnResendOtp.setTextColor(resources.getColor(R.color.color_resend_otp_gray))
                mBtnResendOtp.text = "${getText(R.string.string_otp_resend)}（${mTimeCount + 1}s）"
                mBtnResendOtp.typeface = mNormalTypeface
                mTimeHandler.postDelayed(this, TIME_PERIOD)
            } else {
                mTimeCount = TIME_COUNT
                mBtnResendOtp.isEnabled = true
                mBtnResendOtp.setTextColor(resources.getColor(R.color.color_0072D8))
                mBtnResendOtp.text = getText(R.string.string_otp_resend)
                mBtnResendOtp.typeface = mBoldTypeface
            }
        }
    }


    companion object {
        const val TAG = "OTPActivity"
        const val TIME_COUNT = 60
        const val TIME_PERIOD = 1000L
        private const val PARAM_MOBILE_NUMBER = "mobileNumber"
        private const val PARAM_LOGIN_S_KEY = "login_skey"
        private const val PARAM_LOGIN_SAUTH_ID = "login_sauthid"
        private const val PARAM_USER_NAME = "userName"
        fun startOTPActivity(
            fromActivity: Activity,
            mobileNumber: String? = "",
            loginSkey: String? = "",
            loginSauthId: String? = "",
            userName: String? = ""
        ) {
            val intent = Intent(fromActivity, OTPActivity::class.java)
            intent.putExtra(PARAM_MOBILE_NUMBER, mobileNumber)
            intent.putExtra(PARAM_LOGIN_S_KEY, loginSkey)
            intent.putExtra(PARAM_LOGIN_SAUTH_ID, loginSauthId)
            intent.putExtra(PARAM_USER_NAME, userName)
            fromActivity.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        intent?.let {
            mMobileNumber = it.getStringExtra(PARAM_MOBILE_NUMBER)
            mLoginSkey = it.getStringExtra(PARAM_LOGIN_S_KEY)
            mLoginSauthId = it.getStringExtra(PARAM_LOGIN_SAUTH_ID)
            mUserName = it.getStringExtra(PARAM_USER_NAME)
        }
        initView()
    }


    private fun initView() {
        mTvTitle = R.id.id_tv_title.getTextView()
        mTvSubTitle = R.id.id_tv_sub_tips.getTextView()
        mPasswordInputView = R.id.id_input_password_view.getPasswordInputView()
        mImageEye = R.id.id_img_eye.getImageView()
        mTvError = R.id.id_tv_error.getTextView()
        mBtnResendOtp = R.id.id_btn_resend_otp.getTextView()
        mTvStaticTips = R.id.id_tv_static_tips.getTextView()
        mBtnSubmit = R.id.id_btn_submit.getButton()

        mImageEye.setOnClickListener(this)
        mBtnResendOtp.setOnClickListener(this)
        mBtnSubmit.setOnClickListener(this)

        mPInputImp = mPasswordInputView as PasswordInputImp

        //默认显示密文
        mPInputImp.setCipher(true)

        //已进入就默认发送一条
        sendOrCheckOtp()

        //手机号后四位
        showMobileNumber()

        setTips(isShow = false)

        //行间距
        mTvSubTitle.setLineSpacing(20f, 1f)
        mTvError.setLineSpacing(20f, 1f)


        //间距
        val layoutParam = mPasswordInputView.layoutParams as RelativeLayout.LayoutParams
        layoutParam.rightMargin = KotlinUtils.dp2x(this, 25) + 10 //px
        mPasswordInputView.layoutParams = layoutParam


        //字体设置
        setTypeface()

    }


    /**
     * 字体设置
     */
    private fun setTypeface() {
        mTvTitle.typeface = mBoldTypeface
        mTvSubTitle.typeface = mNormalTypeface
        mTvError.typeface = mBoldTypeface
        mBtnResendOtp.typeface = mBoldTypeface
        mTvStaticTips.typeface = mNormalTypeface
        mBtnSubmit.typeface = mBoldTypeface
    }


    /**
     * 展示手机号后四位
     */
    private fun showMobileNumber() {
        var phone = ""
        mMobileNumber?.let {
            if (!TextUtils.isEmpty(it)) {
                phone = if (it.length <= 4) it else it.substring(it.length - 4, it.length)
            }
        }
        mTvSubTitle.text =
            getString(R.string.string_otp_tips_sub_title).replace(OtpManager.PARAM_SPLIT, phone)
    }


    /**
     * 倒计时60s
     */
    private fun timeCount() {
        mTimeHandler.postDelayed(mTimeRunnable, TIME_PERIOD)
    }


    /**
     * 验证otp
     */
    private fun checkOtp() {
        val otp = mPInputImp.getInputPassword()
        if (!TextUtils.isEmpty(otp) && otp.length > 5) {
            sendOrCheckOtp(otp, true)
        } else {
            setTips(getString(R.string.string_enter_otp), isError = true, isShow = true)
        }
    }


    /**
     * 调用 发送 或 验证 otp接口
     */
    private fun sendOrCheckOtp(
        otp: String = "",
        isCheckOtp: Boolean = false,
    ) {
        if (!isCheckOtp) {
            //发送短信后才开始倒计时
            timeCount()
        }
        OtpManager.sendOrCheckOtp(otp,
            isCheckOtp,
            mLoginSkey,
            mLoginSauthId,
            object : UINotifyListener<OtpEntity>() {
            override fun onPreExecute() {
                super.onPreExecute()
                showLoading(false, ToastHelper.toStr(R.string.loading))
            }


            override fun onError(error: Any?) {
                super.onError(error)
                dismissLoading()
                error?.let {
                    val e = it.toString()
                    if (e.contains(OtpManager.OTP_ERROR_CODE_VERIFY_CODE_ERROR.toString())) {
                        //验证码错误
                        val message =
                            e.replace(OtpManager.OTP_ERROR_CODE_VERIFY_CODE_ERROR.toString(), "")
                        runOnUiThread {
                            setTips(message, isError = true, isShow = true)
                        }
                    } else if (e.contains(OtpManager.OTP_ERROR_CODE_OTP_EXPIRED.toString())) {
                        val message =
                            e.replace(OtpManager.OTP_ERROR_CODE_OTP_EXPIRED.toString(), "")
                        runOnUiThread {
                            setTips(message, isError = true, isShow = true)
                        }
                    } else if (e.contains(OtpManager.OTP_ERROR_CODE_DEVICE_EXCEED_BINDING_LIMIT.toString())) {
                        //设备超过绑定上限
                        val message = e.replace(
                            OtpManager.OTP_ERROR_CODE_DEVICE_EXCEED_BINDING_LIMIT.toString(),
                            ""
                        )
                        displayDialog(message)
                    } else if (e.contains(OtpManager.OTP_ERROR_CODE_SEND_COUNT_EXCEED_LIMIT.toString())) {
                        //发送次数到达上限
                        val message = e.replace(
                            OtpManager.OTP_ERROR_CODE_SEND_COUNT_EXCEED_LIMIT.toString(),
                            ""
                        )
                        displayDialog(message)
                    } else if (e.contains(OtpManager.OTP_ERROR_CODE_VERIFY_CODE_COUNT_EXCEED_LIMIT.toString())) {
                        //验证码错误次数达到上限
                        val message = e.replace(
                            OtpManager.OTP_ERROR_CODE_VERIFY_CODE_COUNT_EXCEED_LIMIT.toString(),
                            ""
                        )
                        displayDialog(message)
                    } else {
                        displayDialog(e, false)
                    }
                }
            }

            override fun onSucceed(result: OtpEntity?) {
                super.onSucceed(result)
                dismissLoading()
                result?.let {
                    if (isCheckOtp) {
                        //验证otp
                        if (it.isSuccess) {
                            //验证成功, 进入主页
                            loginSuccessToMainPage()
                        }
                    } else {
                        //发送otp
                        if (it.isSuccess) {
                            //发送成功
                            if (!TextUtils.isEmpty(it.remainCount)) {
                                //剩余次数
                                runOnUiThread {
                                    setTips(it.message, isError = false, isShow = true)
                                }
                            }
                        }
                    }
                }
            }
        })
    }


    private fun displayDialog(message: String = "", isFinish: Boolean = true) {
        if (isFinish) {
            toastDialog(
                getActivity(), message,
                getString(R.string.string_otp_dialog_ok),
            ) {
                finish()
            }
        } else {
            toastDialog(getActivity(), message, getString(R.string.string_otp_dialog_ok), null)
        }
    }


    /**
     * 进入主页
     */
    private fun loginSuccessToMainPage() {
        //加载保存登录用户信息
        PreferenceUtil.commitString("user_name", mUserName)
        MainApplication.userName = mUserName
        PreferenceUtil.commitString("login_skey", mLoginSkey)
        PreferenceUtil.commitString("login_sauthid", mLoginSauthId)

        //主动做一次蓝牙连接
        try {
            connentBlue()
        } catch (e: Exception) {
            Log.e(TAG, Log.getStackTraceString(e))
        }
        loadPayType()

        startActivity(Intent(OTPActivity@ this, spayMainTabActivity::class.java))
        finish()
    }


    private fun getActivity(): Activity {
        return this
    }


    private fun setTips(msg: String? = "", isError: Boolean = false, isShow: Boolean = false) {
        if (isShow) {
            //展示错误提示
            mTvError.visibility = View.VISIBLE
            mTvError.text = if (TextUtils.isEmpty(msg)) "" else msg
            if (isError) {
                mTvError.setTextColor(resources.getColor(R.color.color_pword_frame_red))
                mTvError.typeface = mNormalTypeface
                mPInputImp.setFrameColor(resources.getColor(R.color.color_pword_frame_red))
                mPInputImp.clearInput()
            } else {
                mTvError.setTextColor(resources.getColor(R.color.text_item_color))
                mTvError.typeface = mBoldTypeface
            }
        } else {
            mTvError.visibility = View.GONE
        }
    }


    override fun onClick(view: View?) {
        view?.let { v ->
            when (v.id) {
                R.id.id_img_eye -> {
                    //点击眼睛
                    exchangeEye()
                }
                R.id.id_btn_resend_otp -> {
                    //点击重发
                    sendOrCheckOtp()
                    mPInputImp.clearInput()
                }
                R.id.id_btn_submit -> {
                    //点击提交
                    checkOtp()

                }
                else -> {}
            }
        }
    }


    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setTitleBarBg(resources.getColor(R.color.white))
        titleBar.setLeftButtonResVisible(true, R.drawable.icon_data_previous)
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onRightLayClick() {}
            override fun onRightButtonClick() {}
            override fun onRightButLayClick() {}
            override fun onLeftButtonClick() {
                finish()
            }
        })
    }


    /**
     * 是否显示密文
     */
    private fun exchangeEye() {
        mIsOpenEye = !mIsOpenEye
        if (!mIsOpenEye) {
            //显示密文
            mPInputImp.setCipher(true)
            mImageEye.setImageResource(R.drawable.icon_eye_close_gray)
        } else {
            //显示明文
            mPInputImp.setCipher(false)
            mImageEye.setImageResource(R.drawable.icon_eye_open_gray)
        }
    }


    private fun Int.getTextView() = findViewById<TextView>(this)
    private fun Int.getButton() = findViewById<Button>(this)
    private fun Int.getImageView() = findViewById<ImageView>(this)
    private fun Int.getPasswordInputView() = findViewById<PasswordInputView>(this)


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mTimeHandler != null && mTimeRunnable != null) {
            mTimeHandler.removeCallbacks(mTimeRunnable)
        }
    }

}