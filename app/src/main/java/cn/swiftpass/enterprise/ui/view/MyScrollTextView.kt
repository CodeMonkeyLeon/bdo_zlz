package cn.swiftpass.enterprise.ui.view

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ScrollView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import cn.swiftpass.enterprise.intl.R

class MyScrollTextView : ConstraintLayout {

    companion object {
        const val TAG = "MyScrollTextView"
    }

    private lateinit var mContext: Context
    private lateinit var mScrollView: ScrollView
//    private lateinit var mTvText: TextView
//    private lateinit var mScrollBar: MyScrollBar

    private var mText = ""
    private var mTextColor = 0


    //字体样式
    private val mBoldTypeface by lazy {
        Typeface.createFromAsset(mContext.assets, "Nunito-ExtraBold.ttf")
    }
    private val mNormalTypeface by lazy {
        Typeface.createFromAsset(mContext.assets, "Nunito-Regular.ttf")
    }


    constructor(context: Context) : super(context) {
        mContext = context
        initView()
    }


    constructor(
        context: Context,
        attributeSet: AttributeSet
    ) : super(context, attributeSet) {
        mContext = context
        initView()
    }


    constructor(
        context: Context,
        attributeSet: AttributeSet,
        def: Int
    ) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
        initView()
    }


    private fun initView() {
        //加载布局
        LayoutInflater.from(mContext).inflate(R.layout.layout_my_scroll_text_view, this)
        //绑定控件
        mScrollView = findViewById(R.id.id_scroll_view)
//        mTvText = findViewById(R.id.id_tv_text)
//        mScrollBar = findViewById(R.id.id_scroll_bar)
//        mTvText.text = mText
//        mTvText.setTextColor(mTextColor)
//        //滑动监听事件
//        initScrollEvent()


        R.id.id_tv_app_info_title.setTextTypeFaceById(mBoldTypeface)
        R.id.id_tv_app_info_content.setTextTypeFaceById(mNormalTypeface)
        R.id.id_tv_push_title.setTextTypeFaceById(mBoldTypeface)
        R.id.id_tv_push_content.setTextTypeFaceById(mNormalTypeface)
    }


    private fun Int.setTextTypeFaceById(typeface: Typeface) {
        findViewById<TextView>(this).typeface = typeface
    }


//    private fun initScrollEvent() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            //高于android6.0才能滑动监听
//            mTvText.viewTreeObserver.addOnGlobalLayoutListener(object :
//                ViewTreeObserver.OnGlobalLayoutListener {
//                override fun onGlobalLayout() {
//                    val visibleHeight = mScrollView.height
//                    val totalHeight = mTvText.layout.height
//                    val textHeight = totalHeight - visibleHeight
//                    mScrollView.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
//                        //位移比例
//                        val offsetRate = scrollY / textHeight.toFloat()
////                        mScrollBar.setOffsetRate(offsetRate)
//                    }
//                    mTvText.viewTreeObserver.removeOnGlobalLayoutListener(this)
//                }
//            })
//        }
//    }


//    fun setTypeface(typeface: Typeface) {
//        mTvText.typeface = typeface
//    }


}