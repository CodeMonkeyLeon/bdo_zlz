package cn.swiftpass.enterprise.ui.activity.otp.entity

data class IsCheckOtpEntity(
    val isCheckOtp: Boolean = false,
    val mobileNumber: String? = ""
) : java.io.Serializable