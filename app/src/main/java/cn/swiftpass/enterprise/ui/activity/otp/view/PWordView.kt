package cn.swiftpass.enterprise.ui.activity.otp.view

import android.content.Context
import android.content.res.TypedArray
import android.graphics.*
import android.os.Handler
import android.text.TextUtils
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.activity.otp.interfaces.PWordCallback
import cn.swiftpass.enterprise.ui.activity.otp.interfaces.PWordImp
import cn.swiftpass.enterprise.utils.KotlinUtils
import cn.swiftpass.enterprise.utils.Utils


/**
 * @author lizheng.zhao
 * @date 2022/12/28
 */
class PWordView : View, PWordImp {


    private var mContext: Context
    private lateinit var mCanvas: Canvas

    /**
     * 编号
     */
    private var mIndex: Int = 0

    //控件对外属性
    private var mTextColor = 0
    private var mFrameColor = 0
    private var mCursorColor = 0
    private var mTextSize = 0

    //整个view的高宽
    private var mViewW = 0
    private var mViewH = 0

    //方框的高宽
    private var mRectW = 0
    private var mRectH = 0

    //圆角弧度
    private var mRadius = 8f

    //画笔
    private val mBackgroundPaint = Paint()
    private val mFramePaint = Paint()
    private val mCursorPaint = Paint()
    private val mTextPaint = Paint()

    //键盘输入的文字
    private var mText: String = ""


    //是否显示光标
    private var mIsShowCursor = false

    //光标闪烁时间间隔
    private var mCursorTime = 800L


    //光标循环闪烁任务
    private val mTimeHandler = Handler()
    private val mCursorRunnable = object : Runnable {
        override fun run() {
            if (TextUtils.isEmpty(mText)) {
                mIsShowCursor = !mIsShowCursor
                invalidate()
            } else {
                mIsShowCursor = false
            }
            mTimeHandler.postDelayed(this, mCursorTime)
        }
    }


    /**
     * 监听回调
     */
    private var mPWordCallback: PWordCallback? = null

    fun setOnPWordCallback(callback: PWordCallback?) {
        mPWordCallback = callback
    }

    constructor(context: Context) : super(context) {
        mContext = context
        initConfiguration()
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        mContext = context
        initConfiguration(mContext.obtainStyledAttributes(attributeSet, R.styleable.MySplitView))
    }


    constructor(context: Context, attributeSet: AttributeSet?, def: Int) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
        initConfiguration(mContext.obtainStyledAttributes(attributeSet, R.styleable.MySplitView))
    }


    constructor(context: Context, attributeSet: AttributeSet?, def: Int, index: Int) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
        mIndex = index
        initConfiguration(mContext.obtainStyledAttributes(attributeSet, R.styleable.MySplitView))
    }


    /**
     * 初始化对外属性
     */
    private fun initConfiguration(
        array: TypedArray? = null,
        textColor: Int = Color.BLACK,
        frameColor: Int = Color.BLACK,
        cursorColor: Int = Color.BLACK,
        textSize: Int = 18
    ) {
        if (array == null) {
            mTextColor = textColor
            mFrameColor = frameColor
            mCursorColor = cursorColor
            mTextSize = textSize
        } else {
            array.let { typedArray ->
                mTextColor =
                    typedArray.getColor(R.styleable.MySplitView_pWordTextColor, Color.BLACK)
                mFrameColor =
                    typedArray.getColor(R.styleable.MySplitView_pWordFrameColor, Color.BLACK)
                mCursorColor =
                    typedArray.getColor(R.styleable.MySplitView_pWordCursorColor, Color.BLACK)
                mTextSize = typedArray.getInt(R.styleable.MySplitView_pWordTextSize, 38)
            }
        }
    }


    /**
     * 测量整个View高宽
     */
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        mViewW = MeasureSpec.getSize(widthMeasureSpec)
        mViewH = MeasureSpec.getSize(heightMeasureSpec)

        setMeasuredDimension(mViewW, mViewH)
    }


    /**
     * 绘制图案
     */
    override fun onDraw(c: Canvas?) {
        super.onDraw(c)
        c?.let { canvas ->
            mCanvas = canvas
            //计算方框的高宽
//            val minLength = if (mViewW > mViewH) mViewH else mViewW
            mRectW = (mViewW * 0.9).toInt()
            mRectH = (mViewH * 0.9).toInt()
            mRadius = 3f / 24 * mRectW

            //方框左上角的坐标
            val xLeft = ((mViewW - mRectW) / 2).toFloat()
            val yLeft = ((mViewH - mRectH) / 2).toFloat()
            //方框右下角的坐标
            val xRight = xLeft + mRectW
            val yRight = yLeft + mRectH


            //绘制背景
            drawBackground(xLeft, yLeft, xRight, yRight)

            //绘制边框
            drawFrame(xLeft, yLeft, xRight, yRight)

            //绘制文字
            if (TextUtils.equals(mText, PWordImp.CIPHER)) {
                //密文
                drawCipher()
            } else {
                //明文
                drawText(yRight)
            }

            //绘制光标
            drawCursor(xLeft, yLeft, yRight)
        }
    }


    /**
     * 绘制背景
     */
    private fun drawBackground(
        xLeft: Float,
        yLeft: Float,
        xRight: Float,
        yRight: Float
    ) {
        mBackgroundPaint.style = Paint.Style.FILL_AND_STROKE
        mBackgroundPaint.color = Color.WHITE
        mCanvas.drawRect(xLeft, yLeft, xRight, yRight, mBackgroundPaint)
    }


    /**
     * 绘制边框
     */
    private fun drawFrame(
        xLeft: Float,
        yLeft: Float,
        xRight: Float,
        yRight: Float
    ) {
        //方框四个角的坐标
        //(xLeft, yLeft)
        //(xRight, yLeft)
        //(xRight, yRight)
        //(xLeft, yRight)
        mFramePaint.style = Paint.Style.STROKE
        mFramePaint.strokeWidth = KotlinUtils.dp2x(mContext, 1).toFloat()//画笔宽度
        mFramePaint.color = mFrameColor
        mFramePaint.isAntiAlias = true
        val frameLeftPath = Path()
        frameLeftPath.moveTo(xLeft, yRight - mRadius)
        frameLeftPath.lineTo(xLeft, yLeft + mRadius)
        mCanvas.drawPath(frameLeftPath, mFramePaint)
        val frameTopPath = Path()
        frameTopPath.moveTo(xLeft + mRadius, yLeft)
        frameTopPath.lineTo(xRight - mRadius, yLeft)
        mCanvas.drawPath(frameTopPath, mFramePaint)
        val frameRightPath = Path()
        frameRightPath.moveTo(xRight, yLeft + mRadius)
        frameRightPath.lineTo(xRight, yRight - mRadius)
        mCanvas.drawPath(frameRightPath, mFramePaint)
        val frameBottomPath = Path()
        frameBottomPath.moveTo(xRight - mRadius, yRight)
        frameBottomPath.lineTo(xLeft + mRadius, yRight)
        mCanvas.drawPath(frameBottomPath, mFramePaint)
        //圆角
        val rectLT = RectF(xLeft, yLeft, xLeft + 2 * mRadius, yLeft + 2 * mRadius)
        mCanvas.drawArc(rectLT, 180f, 90f, false, mFramePaint)
        val rectRT = RectF(xRight - 2 * mRadius, yLeft, xRight, yLeft + 2 * mRadius)
        mCanvas.drawArc(rectRT, 270f, 90f, false, mFramePaint)
        val rectRB = RectF(xRight - 2 * mRadius, yRight - 2 * mRadius, xRight, yRight)
        mCanvas.drawArc(rectRB, 0f, 90f, false, mFramePaint)
        val rectLB = RectF(xLeft, yRight - 2 * mRadius, xLeft + 2 * mRadius, yRight)
        mCanvas.drawArc(rectLB, 90f, 90f, false, mFramePaint)
    }


    /**
     * 绘制文字
     */
    private fun drawText(yRight: Float) {
        if (!TextUtils.isEmpty(mText)) {
            val xTextCenter = (mViewW / 2).toFloat()
            mTextPaint.textAlign = Paint.Align.CENTER
            mTextPaint.color = mTextColor
            mTextPaint.strokeWidth = 3f
            mTextPaint.isAntiAlias = true
            mTextPaint.style = Paint.Style.FILL_AND_STROKE
            mTextPaint.textSize = Utils.dp2px(mTextSize).toFloat()
            mTextPaint.typeface = Typeface.createFromAsset(mContext.assets, "Nunito-Regular.ttf")
            val metrics = mTextPaint.fontMetrics
            val textH = kotlin.math.abs(metrics.top) + kotlin.math.abs(metrics.bottom)
            val y = (mRectH - textH) / 2
            val yTextCenter = yRight - y - kotlin.math.abs(metrics.bottom)
            mCanvas.drawText(mText, xTextCenter, yTextCenter, mTextPaint)
        }
    }


    /**
     * 绘制密文
     */
    private fun drawCipher() {
        val xCenter = (mViewW / 2).toFloat()
        val yCenter = (mViewH / 2).toFloat()
        mTextPaint.color = mTextColor
        mTextPaint.isAntiAlias = true
        mCanvas.drawCircle(xCenter, yCenter, PWordImp.CIPHER_SIZE, mTextPaint)
    }


    /**
     * 绘制光标
     */
    private fun drawCursor(
        xLeft: Float,
        yLeft: Float,
        yRight: Float
    ) {
        if (mIsShowCursor) {
            mCursorPaint.style = Paint.Style.STROKE
            mCursorPaint.strokeWidth = 3f;//画笔宽度
            mCursorPaint.color = mCursorColor
            mCursorPaint.isAntiAlias = true
            val cursorPath = Path()
            cursorPath.moveTo(xLeft + mRectW / 2, (yLeft + 0.3 * mRectH).toFloat())
            cursorPath.lineTo(xLeft + mRectW / 2, (yRight - 0.3 * mRectH).toFloat())
            mCanvas.drawPath(cursorPath, mCursorPaint)
        }
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.let { e ->
            when (e.action) {
                MotionEvent.ACTION_DOWN -> {
                    //获取焦点
                    mPWordCallback?.let {
                        it.pWordOnTouch(MotionEvent.ACTION_DOWN, mIndex, this)
                    }
                    return true
                }
                MotionEvent.ACTION_UP -> {
                    //展示键盘
                    mPWordCallback?.let {
                        it.pWordOnTouch(MotionEvent.ACTION_UP, mIndex, this)
                    }
                }
                else -> {

                }
            }
        }
        return super.onTouchEvent(event)
    }

    /**
     * 展示光标
     */
    override fun showCursor() {
        if (TextUtils.isEmpty(mText)) {
            mIsShowCursor = true
            mTimeHandler.postDelayed(mCursorRunnable, mCursorTime)
            invalidate()
        }
    }

    /**
     * 隐藏光标
     */
    override fun hideCursor() {
        if (TextUtils.isEmpty(mText)) {
            mIsShowCursor = false
            mTimeHandler.removeCallbacks(mCursorRunnable)
            invalidate()
        }
    }

    /**
     * 获取输入文字
     */
    override fun getText() = mText


    override fun inputText(text: String?) {
        when (text) {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" -> {
                //输入数字
                mText = text
                invalidate()
            }
            "/n" -> {
                //回车
            }
            "" -> {
                //删除
                mText = ""
                invalidate()
            }
            PWordImp.CIPHER -> {
                //密文
                mText = text
                invalidate()
            }
        }
    }

    /**
     * 设置外框颜色
     */
    override fun setFrameColor(color: Int) {
        mFrameColor = color
        invalidate()
    }


    /**
     * 获取序号
     */
    override fun getIndex() = mIndex
}