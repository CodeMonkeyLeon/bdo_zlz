package cn.swiftpass.enterprise.ui.activity.test.view


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.net.ApiConstant
import cn.swiftpass.enterprise.ui.activity.BaseActivity
import cn.swiftpass.enterprise.ui.activity.test.adapter.DomainAdapter
import cn.swiftpass.enterprise.ui.activity.test.entity.DomainEntity
import cn.swiftpass.enterprise.ui.test.interfaces.OnDomainItemClickListener
import cn.swiftpass.enterprise.utils.Constants
import cn.swiftpass.enterprise.utils.PreferenceUtil

class TestActivity : BaseActivity(), View.OnClickListener {


    companion object {
        const val HTTP = "http://"
        const val HTTPS = "https://"

        fun startTestActivity(fromActivity: Activity) {
            fromActivity.startActivity(Intent(fromActivity, TestActivity::class.java))
        }
    }


    private lateinit var mBtnBack: TextView
    private lateinit var mBtnOk: TextView

//    private lateinit var mTvCurrentDomain: TextView

    private lateinit var mEtDomain: EditText

    private lateinit var mEtEkycRegister: EditText
    private lateinit var mEtEkycCheck: EditText

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: DomainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_test)
        initView()
    }


    private fun initView() {
        mBtnBack = findViewById(R.id.id_btn_back)
        mBtnOk = findViewById(R.id.id_btn_ok)
//        mTvCurrentDomain = findViewById(R.id.id_tv_domain_current)
        mEtDomain = findViewById(R.id.id_et_domain)
        mRecyclerView = findViewById(R.id.id_recycler_view)
        mEtEkycRegister = findViewById(R.id.id_et_ekyc_register)
        mEtEkycCheck = findViewById(R.id.id_et_ekyc_check)

        mBtnBack.setOnClickListener(this)
        mBtnOk.setOnClickListener(this)
//        mTvCurrentDomain.setOnClickListener(this)


//        mTvCurrentDomain.text = ApiConstant.BASE_URL_PORT

        initRecyclerView()
        initSelfDomain()
        initEkyc.invoke()
    }


    val initEkyc: () -> Unit = {
        val ekycRegisterUrl = PreferenceUtil.getString(
            Constants.EKYC_REGISTER_URL,
            Constants.EKYC_REGISTER_URL_PRD
        )
        val ekycCheckUrl =
            PreferenceUtil.getString(Constants.EKYC_CHECK_URL, Constants.EKYC_CHECK_URL_PRD)

        mEtEkycRegister.setText(ekycRegisterUrl)
        mEtEkycCheck.setText(ekycCheckUrl)
    }


    private fun initSelfDomain() {

        mEtDomain.setText(ApiConstant.BASE_URL_PORT)

        val selfDomain = PreferenceUtil.getString(Constants.SERVER_ADDRESS_SELF, "")
        if (!TextUtils.isEmpty(selfDomain)) {
            mEtDomain.setText(selfDomain.substring(0, selfDomain.length - 1))
        }
    }


    private fun initRecyclerView() {

        val manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.VERTICAL
        mRecyclerView.layoutManager = manager


        mAdapter = DomainAdapter(this, getDomainList())
        mRecyclerView.adapter = mAdapter
        mAdapter.notifyDataSetChanged()


        mAdapter.setOnDomainItemClickListener(object : OnDomainItemClickListener {
            override fun onItemClick(position: Int, entity: DomainEntity, view: View) {
                mEtDomain.setText(entity.domain)
            }
        })
    }


    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }


    private fun getDomainList(): ArrayList<DomainEntity> {
        val list = ArrayList<DomainEntity>()
        list.add(DomainEntity(MainApplication.config.serverAddr))
        list.add(DomainEntity(MainApplication.config.serverAddrBack))
        list.add(DomainEntity(MainApplication.config.serverAddrCheckout))
        list.add(DomainEntity(MainApplication.config.serverAddrOverseas61))
        list.add(DomainEntity(MainApplication.config.serverAddrDev))
        list.add(DomainEntity(MainApplication.config.serverAddrTest123))
        list.add(DomainEntity(MainApplication.config.serverAddrTest61))
        list.add(DomainEntity(MainApplication.config.serverAddrTestUAT))
        list.add(DomainEntity(MainApplication.config.serverAddrPrd))
        list.add(DomainEntity(MainApplication.config.serverAddrDevJH))
        list.forEach {
            it.domain = it.domain.substring(0, it.domain.length - 1)
        }
        return list
    }


    private fun saveChooseDomain(domain: String) {
        when (domain) {
            MainApplication.config.serverAddr -> {
                //线上环境
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddr
                ApiConstant.pushMoneyUrl = MainApplication.config.pushMoneyUrl
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS
                )
            }
            MainApplication.config.serverAddrBack -> {
                //线上环境
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddrBack
                ApiConstant.pushMoneyUrl = MainApplication.config.pushMoneyUrl
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_BACK
                )
            }
            MainApplication.config.serverAddrCheckout -> {
                //checkout-dev
                ApiConstant.BASE_URL_PORT =
                    MainApplication.config.serverAddrCheckout
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_CHECKOUT
                )
            }
            MainApplication.config.serverAddrOverseas61 -> {
                //61容器化
                ApiConstant.BASE_URL_PORT =
                    MainApplication.config.serverAddrOverseas61
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_OVERSEAS_61
                )
            }
            MainApplication.config.serverAddrDev -> {
                //dev
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddrDev
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_DEV
                )
            }
            MainApplication.config.serverAddrTest123 -> {
                //test123
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddrTest123
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_TEST_123
                )
            }
            MainApplication.config.serverAddrTest61 -> {
                //test61
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddrTest61
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_TEST_61
                )
            }
            MainApplication.config.serverAddrTest63 -> {
                //test63
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddrTest63
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_TEST_63
                )
            }
            MainApplication.config.serverAddrTestUAT -> {
                //testUAT
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddrTestUAT
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_TEST_UAT
                )
            }
            MainApplication.config.serverAddrPrd -> {
                //prd
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddrPrd
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlPrd
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_PRD
                )
            }
            MainApplication.config.serverAddrDevJH -> {
                //dev-jh
                ApiConstant.BASE_URL_PORT = MainApplication.config.serverAddrDevJH
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_DEV_JH
                )
            }
            else -> {
                //自定义域名
                ApiConstant.serverAddrSelf = domain
                ApiConstant.BASE_URL_PORT = domain
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest
                PreferenceUtil.commitString(
                    Constants.SERVER_CONFIG,
                    Constants.SERVER_ADDRESS_SELF
                )
                PreferenceUtil.commitString(
                    Constants.SERVER_ADDRESS_SELF,
                    domain
                )
            }
        }
    }


    override fun onClick(v: View?) {
        v?.let { view ->
            when (view.id) {
                R.id.id_btn_back -> {
                    //返回
                    finish()
                }
                R.id.id_btn_ok -> {
                    //确认


                    //ekyc
                    val ekycRegisterText = mEtEkycRegister.text.toString().trim()
                    val ekycCheckText = mEtEkycCheck.text.toString().trim()

                    if (!TextUtils.isEmpty(ekycRegisterText)) {
                        PreferenceUtil.commitString(Constants.EKYC_REGISTER_URL, ekycRegisterText)
                    }

                    if (!TextUtils.isEmpty(ekycCheckText)) {
                        PreferenceUtil.commitString(Constants.EKYC_CHECK_URL, ekycCheckText)
                    }


                    //域名
                    val domain = mEtDomain.text.trim().toString()
                    if (TextUtils.isEmpty(domain)) {
                        showToast("输入的域名不能为空")
                    } else {
                        if (domain.startsWith(HTTP) || domain.startsWith(HTTPS)) {
                            PreferenceUtil.commitString(
                                Constants.SERVER_ADDRESS_SELF,
                                ""
                            )
                            val d = if (domain.endsWith('/')) domain else "$domain/"
                            saveChooseDomain(d)
                            finish()
                        } else {
                            showToast("请以${HTTP}或${HTTPS}开头")
                        }
                    }
                }

            }
        }
    }
}