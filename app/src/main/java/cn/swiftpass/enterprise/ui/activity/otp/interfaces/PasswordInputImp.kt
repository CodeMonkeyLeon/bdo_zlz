package cn.swiftpass.enterprise.ui.activity.otp.interfaces

/**
 * @author lizheng.zhao
 * @date 2023/12/28
 */
interface PasswordInputImp {

    /**
     * 设置外框颜色
     */
    fun setFrameColor(color: Int)


    /**
     * 设置显示密文
     */
    fun setCipher(isCipher: Boolean)

    /**
     * 获取输入的密码
     */
    fun getInputPassword(): String


    /**
     * 清空密码框
     */
    fun clearInput()

}