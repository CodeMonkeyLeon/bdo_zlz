package cn.swiftpass.enterprise.ui.activity.otp.interfaces

/**
 * @author lizheng.zhao
 * @date 2022/12/28
 */
interface PWordImp {

    companion object {
        const val CIPHER = "•"
        const val CIPHER_SIZE = 11f
    }


    /**
     * 显示光标
     */
    fun showCursor()

    /**
     * 隐藏光标
     */
    fun hideCursor()


    /**
     * 输入内容
     */
    fun getText(): String?


    /**
     * 输入文字
     */
    fun inputText(text: String? = "")


    /**
     * 外框颜色
     */
    fun setFrameColor(color: Int)


    /**
     * 获取序号
     */
    fun getIndex(): Int
}