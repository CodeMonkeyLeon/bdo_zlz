package cn.swiftpass.enterprise.ui.view

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import cn.swiftpass.enterprise.bean.BarPositionEntity
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.utils.KotlinUtils

class MyScrollBar : View {

    private lateinit var mContext: Context

    private var mBarWidth: Int = 0
    private var mBarHeight: Int = 0
    private var mBarColor: Int = 0

    private var mHeight: Int = 0
    private var mWidth: Int = 0

    private var mX = 0.0


    private var mOffsetRate = 0.0


    private val mPaint by lazy {
        Paint().apply {
            style = Paint.Style.FILL
            isAntiAlias = true
        }
    }


    constructor(context: Context) : super(context) {
        mContext = context
    }


    constructor(
        context: Context,
        attributeSet: AttributeSet
    ) : super(context, attributeSet) {
        mContext = context
        initConfiguration(mContext.obtainStyledAttributes(attributeSet, R.styleable.MyScrollBar))
    }


    constructor(
        context: Context,
        attributeSet: AttributeSet,
        def: Int
    ) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
        initConfiguration(mContext.obtainStyledAttributes(attributeSet, R.styleable.MyScrollBar))
    }


    private fun initConfiguration(
        array: TypedArray? = null
    ) {
        array?.run {
            mBarColor = getInt(
                R.styleable.MyScrollBar_barColor,
                mContext.resources.getColor(R.color.color_scrollbar_gray)
            )
            mBarWidth = KotlinUtils.dp2x(mContext, getInt(R.styleable.MyScrollBar_barWidth, 8))
            mBarHeight = KotlinUtils.dp2x(mContext, getInt(R.styleable.MyScrollBar_barHeight, 64))
        }
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        mWidth = MeasureSpec.getSize(widthMeasureSpec)
        mHeight = MeasureSpec.getSize(heightMeasureSpec)
        if (mWidth < mBarWidth) {
            throw IllegalArgumentException("Bar的宽度大于轨道的的宽度")
        }
        mX = (mWidth - mBarWidth) / 2.0

//        Log.i("TAG_ZLZ", "mWidth: $mWidth      mHeight: $mHeight")
//        Log.i("TAG_ZLZ", "mBarWidth: $mBarWidth      mBarHeight: $mBarHeight")
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.run {
            mPaint.color = mBarColor

            val offsetY = mOffsetRate * (mHeight - mBarHeight)

            val point = BarPositionEntity(
                mX.toFloat(),
                offsetY.toFloat(),
                mBarWidth.toFloat(),
                mBarHeight.toFloat()
            )


            drawCircle(
                point.getCircleTopX(),
                point.getCircleTopY(),
                point.getCircleR(),
                mPaint
            )


            drawRect(
                point.getRectLeft(),
                point.getRectTop(),
                point.getRectRight(),
                point.getRectBottom(),
                mPaint
            )


            drawCircle(
                point.getCircleBottomX(),
                point.getCircleBottomY(),
                point.getCircleR(),
                mPaint
            )
        }
    }


    fun setOffsetRate(offsetRate: Float) {
        mOffsetRate = offsetRate.toDouble()
        invalidate()
    }

}