/*
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.VerifyEditText;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 忘记密码
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-14]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class FindPassSecondActivity extends TemplateActivity {

    private TimeCount time;

    private VerifyEditText ed_code1;
    private Button btn_next_step;
    private TextView textView1;
    private ImageView iv_switch_pwd;

    private ForgetPSWBean forgetPSWBean;

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    public static void startActivity(Context context, ForgetPSWBean PSWBean) {
        Intent it = new Intent();
        it.setClass(context, FindPassSecondActivity.class);
        it.putExtra("ForgetPSWBean", PSWBean);
        context.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass_second);

        MainApplication.listActivities.add(this);
        initView();

        setLister();
        btn_next_step.getBackground().setAlpha(102);
        forgetPSWBean = (ForgetPSWBean) getIntent().getSerializableExtra("ForgetPSWBean");
        if (null != forgetPSWBean) {
            //进入页面开始倒数
            showToastInfo(R.string.tx_code_send_succ);
            Countdown();
        }
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    /**
     * 验证码倒计时60秒
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void Countdown() {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();
    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            textView1.setEnabled(true);
            textView1.setClickable(true);
            textView1.setText(R.string.bdo_val_otp_not_receive);
            textView1.setTextColor(getResources().getColor(R.color.bg_text_new));
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            textView1.setEnabled(false);
            textView1.setClickable(false);
            textView1.setTextColor(getResources().getColor(R.color.user_edit_color));
            textView1.setText(millisUntilFinished / 1000 + "s");
        }
    }

    private void initView() {
        ed_code1 = getViewById(R.id.ed_code1);
        textView1 = getViewById(R.id.textView1);
        btn_next_step = getViewById(R.id.btn_next_step);
        iv_switch_pwd = getViewById(R.id.iv_switch_pwd);

        iv_switch_pwd.setImageResource(ed_code1.isPwdShow() ?
                R.drawable.icon_open_eye : R.drawable.icon_close_eye);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {
            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (!StringUtil.isEmptyOrNull(ed_code1.getContent().trim())) {
                    setButtonBg(btn_next_step, true, R.string.reg_next_step);
                } else {
                    setButtonBg(btn_next_step, false, R.string.reg_next_step);
                }
            }
        });
        ed_code1.addTextChangedListener(editTextWatcher);
    }

    private final OnFocusChangeListener listener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
        }
    };

    void loadGetCode() {
        UserManager.getCode(forgetPSWBean.getInput(), new UINotifyListener<ForgetPSWBean>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (object != null) {
                    FindPassSecondActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(FindPassSecondActivity.this,
                                    object.toString(), null);
                        }
                    });
                }
            }

            @Override
            public void onPreExecute() {
                super.onPostExecute();
                loadDialog(FindPassSecondActivity.this, R.string.dialog_message);
            }

            @Override
            public void onSucceed(ForgetPSWBean result) {
                // TODO Auto-generated method stub
                super.onSucceed(result);
                dismissLoading();
                if (null != result) {
                    forgetPSWBean.setEe(result.getEe());
                    forgetPSWBean.setToken(result.getToken());
                    showToastInfo(R.string.tx_code_send_succ);
                    Countdown();

                }
            }
        });

    }

    private void setLister() {

        iv_switch_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ed_code1.switchPwdType();
                iv_switch_pwd.setImageResource(ed_code1.isPwdShow() ?
                        R.drawable.icon_open_eye : R.drawable.icon_close_eye);
            }
        });
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadGetCode();
            }
        });

        btn_next_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtil.isEmptyOrNull(ed_code1.getContent().trim())) {
                    toastDialog(FindPassSecondActivity.this, R.string.tx_ver_code, null);
                    ed_code1.setFocusable(true);
                    return;
                }
                checkPhoneCode();
            }
        });

    }

    void checkPhoneCode() {
        if (forgetPSWBean == null) {
            return;
        }
        ed_code1.closeKeyBoard();

        if (TextUtils.isEmpty(forgetPSWBean.getEe())) {
            FindPassSecondActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(FindPassSecondActivity.this,
                            R.string.tx_email_tx, null);
                }
            });
            return;
        }
        UserManager.checkPhoneCode(forgetPSWBean.getToken(), ed_code1.getContent().trim(),
                new UINotifyListener<Boolean>() {
                    @Override
                    public void onError(final Object object) {
                        super.onError(object);
                        dismissLoading();
                        if (object != null) {
                            FindPassSecondActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    toastDialog(FindPassSecondActivity.this,
                                            object.toString(), null);
                                }
                            });
                        }
                    }

                    @Override
                    public void onPreExecute() {
                        super.onPostExecute();
                        loadDialog(FindPassSecondActivity.this, R.string.reg_next_step_loading);
                    }

                    @Override
                    public void onSucceed(Boolean result) {
                        // TODO Auto-generated method stub
                        super.onSucceed(result);
                        dismissLoading();
                        if (result) {
                            forgetPSWBean.setEmailCode(ed_code1.getContent().trim());
                            FindPassSubmitActivity.startActivity(FindPassSecondActivity.this, forgetPSWBean);
                        }
                    }
                });

    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_find_pass_title);
    }

}
