package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

/**
 * 订单统计历史记录
 * User: Alan
 * Date: 13-12-6
 * Time: 下午3:38
 * To change this template use File | Settings | File Templates.
 */
public class CashierReportSumTable extends TableBase{
    public static final String TABLE_NAME="t_report_sum";
    public static final String COLUMN_TRADENUM="tradeNum";
    public static final String COLUMN_TURNOVER="turnover";
    public static final String COLUMN_REFUND_NUM="refundNum";
    public static final String COLUMN_REFUND_FEE="refundFee";
    public static final String COLUMN_FLAG="flag";
    public static final String COLUMN_ADDTIME="addTime";
    public static final String COLUMN_UID="uId";
    @Override
    public void createTable(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "create table "+TABLE_NAME+"("
                + COLUMN_ID + " INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT  ,"
                + COLUMN_TRADENUM + " INT, "
                + COLUMN_TURNOVER + " long, "
                + COLUMN_REFUND_NUM + " INT, "
                + COLUMN_REFUND_FEE + " long, "
                + COLUMN_FLAG + " INT, "
                + COLUMN_ADDTIME + " LONG, "
                + COLUMN_UID + " LONG "
                +")";
        db.execSQL(SQL_CREATE_TABLE);
    }
}
