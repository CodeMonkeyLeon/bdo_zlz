package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

/**
 * 国家与货币单位
 * 
 * @author  Administrator
 * @version  [版本号, 2015-8-25]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class CountryAndUnitTable extends TableBase
{
    
    public static final String TABLE_NAME = "tb_country_unit";
    
    public static final String COLUMN_COUNTRY_ID = "country_id";
    
    public static final String COLUMN_COUNTRY_NAME = "country_name";
    
    public static final String COLUMN__UNIT_ID = "unit_id";
    
    public static final String COLUMN__UNIT_NAME = "unit_name";
    
    @Override
    public void createTable(SQLiteDatabase db)
    {
        final String SQL_CREATE_TABLE =
            "create table " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT  ,"
                + COLUMN_COUNTRY_ID + " INT ," + COLUMN__UNIT_ID + " INT ," + COLUMN_COUNTRY_NAME + " VARCHAR(50) ,"
                + COLUMN__UNIT_NAME + " VARCHAR(20) " + ")";
        
        db.execSQL(SQL_CREATE_TABLE);
    }
    
}
