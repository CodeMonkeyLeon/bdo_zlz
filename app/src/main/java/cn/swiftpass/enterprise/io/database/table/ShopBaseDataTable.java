/*
 * 文 件 名:  ShopBaseData.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;
import cn.swiftpass.enterprise.utils.Logger;

/**
 * 商户 行业基本数据
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ShopBaseDataTable extends TableBase
{
    // 商户 行业基本数据
    public static final String TABLE_NAME = "t_shop_baseData";
    
    //    public static final String COLUMN_MERCHANT_TYPEID = "merchantTypeId";
    
    public static final String COLUMN_TYPE_NAME = "name";
    
    public static final String COLUMN_PARENT_ID = "typeno";
    
    //    public static final String COLUMN_ID = "uId";
    
    @Override
    public void createTable(SQLiteDatabase db)
    {
        final String SQL_CREATE_TABLE =
            "create table " + TABLE_NAME + "(" + COLUMN_TYPE_NAME + " VARCHAR(50)," + COLUMN_PARENT_ID
                + " VARCHAR(50))";
        Logger.i(SQL_CREATE_TABLE);
        db.execSQL(SQL_CREATE_TABLE);
        
    }
    
}
