package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;
import cn.swiftpass.enterprise.utils.Logger;

/**
 *
 * User: Administrator
 * Date: 13-10-13
 * Time: 下午6:11
 * To change this template use File | Settings | File Templates.
 */
public class UserInfoTable extends TableBase
{
    public static final String TABLE_NAME = "t_user";
    
    public static final String COLUMN_REAL_NAME = "real_name";
    
    public static final String COLUMN_USER_NAME = "user_name";
    
    public static final String COLUMN_USER_PWD = "password";
    
    public static final String COLUMN_M_ID = "merchant_id";
    
    public static final String COLUMN_M_NAME = "merchant_name";
    
    public static final String COLUMN_USER_OPTION = "user_option_type";
    
    public static final String COLUMN_USER_TYPE = "user_type";
    
    public static final String COLUMN_ADD_TIME = "add_time";
    
    public static final String COLUMN_LOGIN_TIME = "login_time";
    
    public static final String COLUMN_DERVICE_NO = "dervice_no";
    
    public static final String COLUMN_U_ID = "uId";
    
    public static final String COLUMN_EXIT = "existData";
    
    @Override
    public void createTable(SQLiteDatabase db)
    {
        //
        final String SQL_CREATE_TABLE =
            "create table " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT  ,"
                + COLUMN_U_ID + " INTEGER    ," + COLUMN_REAL_NAME + " VARCHAR(50)," + COLUMN_USER_NAME
                + " VARCHAR(50)," + COLUMN_M_ID + " VARCHAR(50)," + COLUMN_M_NAME + " VARCHAR(50)," + COLUMN_USER_PWD
                + " VARCHAR(50)," + COLUMN_DERVICE_NO + " VARCHAR(50)," + COLUMN_ADD_TIME + " LONG ,"
                + COLUMN_LOGIN_TIME + " LONG ," + COLUMN_USER_OPTION + " int ," + COLUMN_USER_TYPE + " int ,"
                + COLUMN_EXIT + " int )";
        Logger.i(SQL_CREATE_TABLE);
        db.execSQL(SQL_CREATE_TABLE);
        
    }
}
