package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

public abstract class TableBase
{
	public static final String COLUMN_ID    = "id";
	
	public abstract void createTable(SQLiteDatabase db);
}