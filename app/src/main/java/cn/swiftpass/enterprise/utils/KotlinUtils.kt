package cn.swiftpass.enterprise.utils

import android.content.Context
import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.DynModel

object KotlinUtils {


    val getPaymentType: (String?) -> String? = { apiCode ->
        if (TextUtils.equals(apiCode, "62")) {
            "Instapay"
        } else MainApplication.getPayTypeMap().get(apiCode)
    }


    /**
     * dp转换为px
     */
    fun dp2x(context: Context, dipValue: Int): Int {
        val density: Float = context.resources.displayMetrics.density
        return (dipValue * density + 0.5f).toInt()
    }

    /**
     * px转换为dp
     */
    fun px2dp(context: Context, pxValue: Int): Int {
        val density: Float = context.resources.displayMetrics.density
        return (pxValue / density + 0.5f).toInt()
    }


    fun sortByApiCode(list: List<DynModel>): List<DynModel> {
        list.forEach {
            it.setSortWeight(it.apiCode)
        }
        val newList = list.sortedBy { it.sortWeight }
        return newList
    }


}