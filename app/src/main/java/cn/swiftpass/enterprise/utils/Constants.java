package cn.swiftpass.enterprise.utils;

public class Constants {

    public static final String SP_DATA_UUID = "uuid";

    public static final String BDO = "bdo";

    public static final int PAY_TYPE_NOT_IN_SUPPORT_LIST = -1;


    public static final String SERVER_ADDRESS_SELF = "self";
    public static final String SERVER_CONFIG = "serverCifg";

    public static final String SERVER_ADDRESS = "add";

    public static final String SERVER_ADDRESS_BACK = "addBack";
    public static final String SERVER_ADDRESS_PRD = "prd";
    public static final String SERVER_ADDRESS_TEST_123 = "test123";
    public static final String SERVER_ADDRESS_TEST_61 = "test61";
    public static final String SERVER_ADDRESS_TEST_63 = "test63";
    public static final String SERVER_ADDRESS_TEST_UAT = "testUAT";
    public static final String SERVER_ADDRESS_DEV = "dev";
    public static final String SERVER_ADDRESS_DEV_JH = "dev-jh";
    public static final String SERVER_ADDRESS_CHECKOUT = "checkout";
    public static final String SERVER_ADDRESS_OVERSEAS_61 = "overseas61";

    public static final int EDIT_MOBILE_NUMBER_REQUEST_CODE = 6666;
    public static final int EDIT_MOBILE_NUMBER_RESULT_CODE = 7777;


    public static final String EKYC_REGISTER_URL = "ekyc_register_url";
    public static final String EKYC_CHECK_URL = "ekyc_check_url";

    public static final String EKYC_REGISTER_URL_PRD = "https://www.bdocheckout.bdo.com.ph/signup/moblie-index.html#/index";

    public static final String EKYC_CHECK_URL_PRD = "https://www.bdocheckout.bdo.com.ph/signup/moblie-index.html#/check-mid-process";
}
