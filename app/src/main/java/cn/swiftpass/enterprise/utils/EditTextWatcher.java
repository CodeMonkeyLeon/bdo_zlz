package cn.swiftpass.enterprise.utils;

import android.text.Editable;
import android.text.TextWatcher;

public class EditTextWatcher implements TextWatcher
{
	public interface OnTextChanged
	{
		void onExecute(CharSequence s, int start, int before, int count);
		void onAfterTextChanged(Editable s);
	}
	private OnTextChanged mTextChangedListener;
	public void setOnTextChanaged(OnTextChanged onTextChanged)
	{
		mTextChangedListener = onTextChanged;
	}
	
	@Override
	public void afterTextChanged(Editable s)
	{
		if(null != mTextChangedListener){
			mTextChangedListener.onAfterTextChanged(s);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after)
	{
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		if(null != mTextChangedListener){
			mTextChangedListener.onExecute( s,  start,  before,  count);
		}
	}
}
