package cn.swiftpass.enterprise.interfaces

interface OnPrivacyDialogClickListener {

    fun onCloseClick()

    fun onAgreeClick()

    fun onPrivacyClick()

}